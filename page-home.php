<?php
/**
* Template name: Home Page
*/
get_header();
global $post;
$postid = $post->ID;
?>
<!-- Banner Main
================================================== -->
<!--<div class="banner-main banner2">
  <div class="flexslider flexslider1">
    <ul class="slides">
      <?php
      $args = array(
      'post_type' => 'slider',
      'orderby'   => 'menu_order',
      'order'     => 'ASC'
      );
      $the_query = new WP_Query( $args );
      // The Loop
      if ( $the_query->have_posts() ) {
      while ( $the_query->have_posts() ) {
      $the_query->the_post();
      $image = get_field('slider_image');
      if( !empty($image) ):
      $url = $image['url'];
      $title = $image['title'];
      $alt = $image['alt'];
      $image = "<img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" />";
      else:
      $image = "<img src=\"" . get_template_directory_uri() . "/images/banner-wilson-01.jpg\" alt=\"PHOTO\" />";
      endif;
      ?>
      <li>
        <?php if(get_field('make_the_slide_link')): ?>
        <a href="<?php echo get_field('slider_link'); ?>">
          <?php endif; ?>
          <figure>
            <div class="statment-content">
              <div class="position-statement">
                <h1>
                <?php if(get_field('slider_position_statement')): ?>
                "<?php echo get_field('slider_position_statement'); ?>"
                <?php endif; ?>
                </h1>
                <?php if(get_field('slider_position_statement_paragraph')): ?>
                <?php echo get_field('slider_position_statement_paragraph'); ?>
                <?php endif; ?>

              </div>
              <div class="statment-content-icons">

                <img src="<?php echo get_field('slider_position_statement_image'); ?>">

              </div>
            </div>

            <?php echo $image; ?>
          </figure>
          <div class="banner-info-bg">
            <div class="banner-info">
              <div class="row">
                <div class="large-12 columns">
                  <div class="txt-bg">
                    <aside>
                      <h2><?php echo get_field('slider_header_title'); ?></h2>
                      <p><?php echo get_field('slider_header_text'); ?></p>
                    </aside>
                    <a href="#" class="curve-bg">Discover our Products</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php if(get_field('make_the_slide_link')): ?>
        </a>
        <?php endif; ?>
      </li>
      <?php
      }
      }
      ?>

    </ul>
  </div>
</div>-->
<!-- Banner Main
================================================== -->
<div class="banner-main banner2">
  <div class="flexslider flexslider1">

    <ul class="slides">

      <?php
      $args = array(
      'post_type' => 'slider',
      'orderby'   => 'menu_order',
      'order'     => 'ASC'
      );
      $the_query = new WP_Query( $args );
      // The Loop
      if ( $the_query->have_posts() ) {
      while ( $the_query->have_posts() ) {
      $the_query->the_post();
      $image = get_field('slider_image');
      if( !empty($image) ):
      $url = $image['url'];
      $title = $image['title'];
      $alt = $image['alt'];
      $image = "<img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" />";
      else:
      $image = "<img src=\"" . get_template_directory_uri() . "/images/banner-wilson-01.jpg\" alt=\"PHOTO\" />";
      endif;
      ?>
      <li>
        <?php if(get_field('make_the_slide_link')): ?>
        <a href="<?php echo get_field('slider_link'); ?>">
          <?php endif; ?>
        <figure><?php echo $image; ?></figure>

        <div class="banner-txt">
          <div class="outer">
            <div class="inner">
              <div class="row">
                <div class="large-12 columns">

                  <article>

                    <div class="cont-txt">
                      <aside>
                        <h2>
                        <?php if(get_field('slider_position_statement')): ?>
                        "<?php echo get_field('slider_position_statement'); ?>"
                        <?php endif; ?>
                        </h2>
                        <?php if(get_field('slider_position_statement_paragraph')): ?>
                        <p><?php echo get_field('slider_position_statement_paragraph'); ?></p>
                        <?php endif; ?>
                      </aside>

                       <?php if( have_rows('slider_position_statement_logo') ):?>
                      <ul>
                        <?php while ( have_rows('slider_position_statement_logo') ) : the_row();?>
                        <li><img src="<?php echo the_sub_field('slider_position_logo'); ?>" alt="UNILEVER" /></li>
                        <?php endwhile; ?>
                      </ul>
                      <?php endif; ?>
                    </div>
                  </article>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="banner-info-bg">
          <div class="banner-info">
            <div class="row">
              <div class="large-12 columns">
                <div class="txt-bg">
                  <aside>
                    <h2><?php echo get_field('slider_header_title'); ?></h2>
                    <p><?php echo get_field('slider_header_text'); ?></p>
                  </aside>
                  <a href="#" class="curve-bg">Discover our Products</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php if(get_field('make_the_slide_link')): ?>
      </a>
      <?php endif; ?>
    </li>
    <?php
    }
    }
    ?>

  </ul>

</div>
</div>


<?php get_template_part('content','products-list'); ?>


<!-- Section - 2
================================================== -->
<div class="section-2">
<div class="row">
  <div class="large-12 columns">
    <div class="heading-txt">
      <h2>Our Partners</h2>
      <p><?php echo get_field('home_page_partners_intro'); ?></p>
    </div>
  </div>
</div>
<div class="partners-list">
  <div class="row">
    <div class="large-12 columns">
      <div id="owl-demo1" class="owl-carousel owl-theme">
        <?php
        $args = array(
        'post_type' => 'partner',
        'orderby'   => 'menu_order',
        'order'     => 'ASC'
        );
        $the_query = new WP_Query( $args );
        // The Loop
        if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
        $the_query->the_post();
        $image = get_field('home_partners_logo');
        if( !empty($image) ):
        $url = $image['url'];
        $title = $image['title'];
        $alt = $image['alt'];
      $image = "<figure><img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" /></figure>";
      else:
    $image = "<figure><img src=\"" . get_template_directory_uri() . "/images/product-wilson-01.jpg\" alt=\"PHOTO\" /></figure>";
    endif;
    ?>
    <div class="item">
      <article>
        <?php echo $image; ?>
        <a href="<?php echo get_field('partners_discover_link'); ?>" target="_blank" class="btn-secondary">Discover more</a>
      </article>
    </div>
    <?php
    }
    }
    ?>
  </div>
</div>
</div>
<?php if($the_query->found_posts > 6): ?>
<div class="navigation">
<a class="previous prevp">Previous</a>
<a class="next nextn">Next</a>
</div>
<?php endif; ?>
</div>
</div>
<!-- Section - 3
================================================== -->
<div class="section-3">
<div class="row">
<div class="large-12 columns">
<div class="panel-list">
  <article>
  <figure><img src="<?php echo get_template_directory_uri(); ?>/images/icon-panel-01.png" alt="icon" /></figure>
  <aside>
    <h3><span>Cost</span> Efficient</h3>
    <p><?php echo get_field('home_page_cost_efficient', $postid); ?></p><br />
    <a href="/cost-efficient/" class="btn-secondary">Read more</a>
  </aside>
</article>
<article>
<figure><img src="<?php echo get_template_directory_uri(); ?>/images/icon-panel-02.png" alt="icon" /></figure>
<aside>
  <h3><span>Energy</span> Efficient</h3>
  <p><?php echo get_field('home_page_energy_efficient', $postid); ?></p><br />
  <a href="/energy-efficient/" class="btn-secondary">Read more</a>
</aside>
</article>
<article>
<figure><img src="<?php echo get_template_directory_uri(); ?>/images/icon-panel-03.png" alt="icon" /></figure>
<aside>
<h3><span>Renewables</span> Integration</h3>
<p><?php echo get_field('home_page_renewables_integration', $postid); ?></p><br />
<a href="/renewables-integration/" class="btn-secondary">Read more</a>
</aside>
</article>
<article>
<figure><img src="<?php echo get_template_directory_uri(); ?>/images/icon-panel-04.png" alt="icon" /></figure>
<aside>
<h3><span>Need it</span> now</h3>
<p><?php echo get_field('home_page_need_it_now', $postid); ?></p><br />
<a href="/need-it-now/" class="btn-secondary">Read More</a>
</aside>
</article>
</div>
</div>
</div>
</div>
<!-- Section - 4
================================================== -->
<div class="section-4">
<div class="row">
<div class="large-12 columns">
<div class="heading-txt">
<h2>About Us</h2>
<p><?php echo get_field('about_us_intro', $postid); ?></p>
</div>
</div>
</div>
<div class="row">
<div class="large-12 columns">
<div class="product-list">
<article>
<figure><img src="<?php echo get_template_directory_uri(); ?>/images/History-Quick-Link.jpg" alt="PHOTO" /></figure>
<aside>
<div class="cont">
<h3>Our History</h3>
<a href="/our-history/" class="btn">Read More</a>
</div>
</aside>
</article>
<article>
<figure><img src="<?php echo get_template_directory_uri(); ?>/images/photo-wilson-02.jpg" alt="PHOTO" /></figure>
<aside>
<div class="cont">
<h3>Case Studies</h3>
<a href="/case-studies/" class="btn">Read More</a>
</div>
</aside>
</article>
<article>
<figure><img src="<?php echo get_template_directory_uri(); ?>/images/photo-wilson-03.jpg" alt="PHOTO" /></figure>
<aside>
<div class="cont">
<h3>Environmental Commitment</h3>
<a href="/environmental-commitment/" class="btn">Read More</a>
</div>
</aside>
</article>
</div>
</div>
</div>
</div>
<!-- Section - 5
================================================== -->
<div class="section-5">
<div class="row">
<div class="large-12 columns">
<div class="heading-txt">
<h2><?php echo get_field( 'testimonial_title', $postid ); ?></h2>
<p><?php echo get_field( 'testimonial_introduction', $postid ); ?></p>
</div>
</div>
</div>
<div class="testimonials">
<div class="row">
<div class="large-12 columns">
<div class="flexslider flexslider2">
<ul class="slides">
<?php
$args = array(
'post_type' => 'testimonial',
'orderby'   => 'menu_order',
'order'     => 'ASC'
);
$the_query = new WP_Query( $args );
// The Loop
if ( $the_query->have_posts() ) {
while ( $the_query->have_posts() ) {
$the_query->the_post();
$image = get_field('testimonial_logo');
if( !empty($image) ):
$url = $image['url'];
$title = $image['title'];
$alt = $image['alt'];
$image = "<figure><img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" /></figure>";
else:
$image = "<figure><img src=\"" . get_template_directory_uri() . "/images/photo-testi-01.png\" alt=\"PHOTO\" /></figure>";
endif;
?>
<li>
<aside>
<?php echo $image; ?>
<p><?php echo get_field('testimonial_content'); ?></p>
<p class="cl-name"><?php echo get_field('testimonial_name'); ?></p>
<p class="cl-desi"><?php echo get_field('testimonial_company'); ?></p>
</aside>
</li>
<?php
}
}
?>
</ul>
</div>
</div>
</div>
</div>
</div>
<!-- Section - 6
================================================== -->
<div class="section-6">
<div class="row">
<div class="large-12 columns">
<div id="owl-demo2" class="owl-carousel owl-theme">
<?php
$args = array(
'post_type' => 'home_logos',
'orderby'   => 'menu_order',
'order'     => 'ASC'
);
$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
while ( $the_query->have_posts() ) {
$the_query->the_post();
$image = get_field('bottom_logo_image');
if( !empty($image) ):
$url = $image['url'];
$title = $image['title'];
$alt = $image['alt'];
$size = 'product';
$thumb = $image['sizes'][ $size ];
$width = $image['sizes'][ $size . '-width' ];
$height = $image['sizes'][ $size . '-height' ];
$image = "<figure><img src=\"" . $url ."\" alt=\"." . $alt . "\" /></figure>";
else:
$image = "<figure><img src=\"" . get_template_directory_uri() . "/images/logo-iso.gif\" alt=\"PHOTO\" /></figure>";
endif;
?>
<div class="item"><?php echo $image; ?></div>
<?php
}
}
?>
</div>
</div>
</div>
<?php if($the_query->found_posts > 5): ?>
<div class="navigation">
<a class="previous prev2">Previous</a>
<a class="next next2">Next</a>
</div>
<?php endif; ?>
</div>
<?php
get_footer();
?>
</body>
</html>
