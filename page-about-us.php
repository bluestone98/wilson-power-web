<?php
 /**
 * Template name: About Us
 */

	get_header();?>

  
     <!-- Banner Inner
     ================================================== -->
     <div class="banner-innerout">
     <div class="banner-inner">
     <div class="row">

          <div class="large-6 columns">
          	<figure><img  src="<?php the_field('team-image'); ?>" alt=""/></figure>
            <div class="figure-bg">
              <p><img class="service-image" src="<?php echo get_template_directory_uri(); ?>/images/responsible.png"</p><a href="#" class="btn trigger-overlay-about">MEET THE TEAM</a></div>
            </div>
          

          <div class="large-6 columns">
          <aside>
          <p class="txt"><?php the_title();?></p>

          <?php the_field('team-intro-text'); ?>
          </aside>
          </div>

     </div>
     </div>
     </div>
<div class="overlay-new overlay-about overlay-hugeinc black">
	
	<button type="button" class="overlay-close overlay-close-product">Close</button>
	
	<div class="outer-bg">
		<div class="inner-bg">
			<div class="form-block product">
				<div class="form-container group-image-shot">
					<img src="<?php echo get_template_directory_uri(); ?>/images/file-page1.jpg" alt="Team">
				</div>
			</div>
		</div>
	</div>
	
</div>



 <!-- Section - Three cols
     ================================================== -->
     <div class="section-10">
     <div class="row">
     <div class="large-12 columns">
     
          <div class="accord-list">
          <ul>
        <?php $counter = 0; ?>
         <?php

          // check if the repeater field has rows of data
          if( have_rows('accordion_field') ):

            // loop through the rows of data
              while ( have_rows('accordion_field') ) : the_row(); ?>
                  <li>
          <a class="accord-btn"><span><img src="<?php the_sub_field('accordion_image'); ?>"><?php the_sub_field('accordion_title'); ?></span></a>
          <div class="accord-cont">
                    <aside><p>
                     <?php the_sub_field('accordion_content'); ?>
                    </p>
                  </aside>
                  </div>
                  </li>
                  
                  <?php
              endwhile;

          else :

              // no rows found

          endif;

          ?>
         </ul>
          </div>
          
     </div>
     </div>
     </div>



<div class="section-4 grey">
  <div class="row">
    <div class="large-12 columns">
      <div class="heading-txt">
        <h2>Read more about us</h2>
      </div>
      </div>
      </div>
      <div class="row">
      <div class="large-12 columns">
      <div class="product-list">
<?php
// check if the repeater field has rows of data
if( have_rows('about_us_repeater') ):
// loop through the rows of data
while ( have_rows('about_us_repeater') ) : the_row(); ?>
        <article>
        <figure><img src="<?php the_sub_field('about_us_repeater_image'); ?>" alt="PHOTO"></figure>
          <aside>
            <div class="cont">
            <h3><?php the_sub_field('about_us_repeater_header'); ?></h3>
              <a href="<?php the_sub_field('about_us_repeater_link'); ?>" class="btn">Read More</a>
            </div>
          </aside>
        </article>
        <?php
endwhile;
else :
// no rows found
endif;
?>
      </div>
    </div>
  </div>
</div>


<?php get_template_part('content','testimonials'); ?>

<?php
get_footer();
?>
<script type="text/javascript">
(function() {

    var overlayProd = document.querySelector( 'div.overlay-about' );

    if( overlayProd  ) {

        var triggerBttnProd = document.getElementsByClassName( 'trigger-overlay-about' ),
            closeBttnProd = overlayProd.querySelector( 'button.overlay-close-product' );
            transEndEventNames = {
                'WebkitTransition': 'webkitTransitionEnd',
                'MozTransition': 'transitionend',
                'OTransition': 'oTransitionEnd',
                'msTransition': 'MSTransitionEnd',
                'transition': 'transitionend'
            },
            transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
            support = { transitions : Modernizr.csstransitions };

        function toggleOverlay() {
            if( classie.has( overlayProd, 'open' ) ) {

                classie.remove( overlayProd, 'open' );
                classie.add( overlayProd, 'close' );
                var onEndTransitionFn = function( ev ) {
                    if( support.transitions ) {
                        if( ev.propertyName !== 'visibility' ) return;
                        this.removeEventListener( transEndEventName, onEndTransitionFn );
                    }
                    classie.remove( overlayProd, 'close' );
                };
                if( support.transitions ) {
                    overlayProd.addEventListener( transEndEventName, onEndTransitionFn );
                }
                else {
                    onEndTransitionFn();
                }
            }
            else if( !classie.has( overlayProd, 'close' ) ) {
                classie.add( overlayProd, 'open' );
            }
        }

        for(var i=0;i<triggerBttnProd.length;i++){
            triggerBttnProd[i].addEventListener('click', toggleOverlay );
       }

        closeBttnProd.addEventListener( 'click', toggleOverlay );

    }
        
})();
</script>
<script type="text/javascript">
    $('.section-col.about-us:even').find('div.small-12.medium-6.large-6.columns.first-about-us').attr('class','small-12 medium-6 large-6 large-push-6 columns');
  $('.section-col.about-us:even').find('div.small-12.medium-6.large-6.columns.second-about-us').attr('class','small-12 medium-6 large-6 large-pull-6 columns');
  $('.section-col.about-us:odd').css('background','#fff');
</script>
</body>
</html>
