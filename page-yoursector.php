<?php
/**
* Template name: Your Sector
*/
get_header();?>
<section class="your-sector">
	<!-- Banner Inner
	================================================== -->
	<div class="banner-innerout">
		<div class="banner-inner">
			<div class="row">
				<div class="medium-6 columns">
					<img class="hero-image" src="<?php the_field('intro-image'); ?>" alt=""/>
				</div>
				<div class="medium-6 columns">
					<aside>
						<p class="txt"><?php the_title();?></p>
						<?php the_field('your-sector-intro-text'); ?>
					</aside>
				</div>
			</div>
		</div>
	</div>
	<!-- Section - Three cols
	================================================== -->
	<?php
	$counter = 0;
	// check if the repeater field has rows of data
	if( have_rows('different-sectors') ):
	// loop through the rows of data
	while ( have_rows('different-sectors') ) : the_row(); ?>
	<div class="section-col">
		<div class="row">
			<div class="article-cols">
				<div class="large-12 columns">
					<article class="your-sector-intro-image">
					<img class="service-image" src="<?php the_sub_field('sub_field_image'); ?>" /></figure>
					<div class="content">
						<div class="small-12 medium-7 large-8 columns">
							<div class="large-3 columns show-for-large-up">
								<img src="<?php the_sub_field('sub_field_content_icon'); ?>" />
							</div>
							<div class="small-12 medium-12 large-9 columns">
								
								<h2><?php the_sub_field('sub_field_header'); ?></h2>
								<h3><?php the_sub_field('sub_field_header_intro'); ?></h3>
							</div>
						</div>
						<div class="small-12 medium-5 large-4 columns">
							<div class="btn-out">
								<a href="#" class="btn trigger-overlay-<?php echo $counter; ?>" ><span>View Sector</span></a>
							</div>
						</div>
					</div>
				</article>
			</div>
			
		</div>
	</div>
</div>

<div class="overlay-new overlay-<?php echo $counter; ?> overlay-hugeinc black">
	
	<button type="button" class="overlay-close overlay-close-product">Close</button>
	
	<div class="outer-bg">
		<div class="inner-bg">
			<div class="form-block product">
				<div class="form-container">
					
					<div class="row">
						<div class="medium-12 small-12 columns first">
							
							<div class="small-8 columns">
								<div class="modal_header_intro">
									<h2><?php the_sub_field('sub_field_header'); ?></h2>
									<h3><?php the_sub_field('sub_field_header_intro'); ?></h3>
								</div>
							</div>
							<div class="small-4 columns">
							
								<img src="<?php the_sub_field('modal_image'); ?>" />

							</div>
						</div>
						
						<div class="medium-12 columns second">
							<?php the_sub_field('sub_field_content'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
(function() {

    var overlayProd = document.querySelector( 'div.overlay-<?php echo $counter; ?>' );

    if( overlayProd  ) {

        var triggerBttnProd = document.getElementsByClassName( 'trigger-overlay-<?php echo $counter; ?>' ),
            closeBttnProd = overlayProd.querySelector( 'button.overlay-close-product' );
            transEndEventNames = {
                'WebkitTransition': 'webkitTransitionEnd',
                'MozTransition': 'transitionend',
                'OTransition': 'oTransitionEnd',
                'msTransition': 'MSTransitionEnd',
                'transition': 'transitionend'
            },
            transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
            support = { transitions : Modernizr.csstransitions };

        function toggleOverlay() {
            if( classie.has( overlayProd, 'open' ) ) {

                classie.remove( overlayProd, 'open' );
                classie.add( overlayProd, 'close' );
                var onEndTransitionFn = function( ev ) {
                    if( support.transitions ) {
                        if( ev.propertyName !== 'visibility' ) return;
                        this.removeEventListener( transEndEventName, onEndTransitionFn );
                    }
                    classie.remove( overlayProd, 'close' );
                };
                if( support.transitions ) {
                    overlayProd.addEventListener( transEndEventName, onEndTransitionFn );
                }
                else {
                    onEndTransitionFn();
                }
            }
            else if( !classie.has( overlayProd, 'close' ) ) {
                classie.add( overlayProd, 'open' );
            }
        }

        for(var i=0;i<triggerBttnProd.length;i++){
            triggerBttnProd[i].addEventListener('click', toggleOverlay );
       }

        closeBttnProd.addEventListener( 'click', toggleOverlay );

    }
        
})();
</script>

<?php $counter++; ?>
<?php
endwhile;
else :
// no rows found
endif;
?>

<?php get_template_part('content','testimonials'); ?>

<?php
get_footer();
?>
</body>
</html>