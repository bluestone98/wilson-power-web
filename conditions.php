<?php
/**
* Template name: Conditions
*/
get_header();
global $post;
$postid = $post->ID;
?>
<section class="your-sector conditions">
	<!-- Banner Inner
	================================================== -->
	<div class="banner-innerout">
		<div class="banner-inner">
			<div class="row">
				<div class="medium-12 columns">
					<aside>
						<?php the_field('your-sector-intro-text'); ?>
					</aside>
				</div>
			</div>
		</div>
	</div>

	<!-- Section - Three cols
	================================================== -->
	<?php
	$counter = 0;
	// check if the repeater field has rows of data
	if( have_rows('different-sectors') ):
	// loop through the rows of data
	while ( have_rows('different-sectors') ) : the_row(); ?>
	<div class="section-col">
		<div class="row">
			<div class="article-cols">
				<div class="large-12 columns">
					<article class="services">

						<div class="small-12 medium-6 large-6 columns first">
								<img class="service-image" src="<?php the_sub_field('sub_field_image'); ?>" /></figure>
						</div>
						<div class="small-12 medium-6 large-6 columns second">
							<?php the_sub_field('sub_field_content'); ?>
						</div>

				</article>
			</div>
			
		</div>
	</div>
</div>
<?php $counter++; ?>
<?php
endwhile;
else :
// no rows found
endif;
?>

<!-- Section - 5
================================================== -->
<?php get_template_part('content','testimonials'); ?>


<?php
get_footer();
?>


<script type="text/javascript">
	
	$('.your-sector .section-col:even').find('div.small-12.medium-6.large-6.columns.first').attr('class','small-12 medium-6 large-6 large-push-6 columns');
	$('.your-sector .section-col:even').find('div.small-12.medium-6.large-6.columns.second').attr('class','small-12 medium-6 large-6 large-pull-6 columns');

</script>
</body>
</html>