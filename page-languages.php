<?php
/**
* Template name: Languages Landing Pages
*/
get_header();?>
<!-- Banner Inner
================================================== -->
<div class="section-land">
	<div class="row">
		<div class="large-12 columns landing-pages">
			
			<div class="article-out">
				<article>
					
					<aside>
						<h2>British Quality</h2>
						<h3>Power &amp; Distribution transformer solutions</h3>
						<p>Quick turnaround, manufactured to the highest international standards.</p>
					</aside>
				</article>
			<figure><img src="<?php echo get_template_directory_uri(); ?>/images/product-wilson-09.png" alt="PHOTO" /></figure>
		</div>
		
		<div class="asidetxt-out">
			<div class="asidetxt-in">
			<?php 

				$image = get_field('languages_specific_language');

				if( !empty($image) ): ?>
				<div class="icon-flag"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" alt="Flag" /></div>
				<?php endif; ?>
				<aside>
					<p><?php the_field('languages_specific_intro'); ?></p>
				</aside>
			</div>
		</div>
		
	</div>
</div>
</div>
<!-- Section - Three cols
================================================== -->
<div class="section-grey">
<div class="row">
	<div class="large-12 columns">
		<article>
			<aside>
			<?php 

			$image = get_field('languages_team_image');

			if( !empty($image) ): ?>
			<figure><img class="languages_team" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></figure>
			<?php endif; ?>
			<h3><?php the_field('languages_team_h2'); ?></h3>
			<?php the_field('languages_team_p'); ?>
			
		</aside>
		<aside class="aside-right">
			<p><?php the_field('languages_team_second_p'); ?></p>
		</aside>
	</article>
	
</div>
</div>
</div>

<?php
get_footer();
?>
</body>
</html>