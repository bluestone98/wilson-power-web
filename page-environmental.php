<?php
/**
* Template name: Environmental Commitment
*/
get_header();?>
<section class="your-sector">
	<!-- Banner Inner
	================================================== -->
	<div class="banner-innerout">
		<div class="banner-inner">
			<div class="row">
				<div class="medium-6 columns">
					<img class="hero-image" src="<?php the_field('intro-image'); ?>" alt=""/>
				</div>
				<div class="medium-6 columns">
					<aside>
						<p class="txt"><?php the_title();?></p>
						<?php the_field('your-sector-intro-text'); ?>
					</aside>
				</div>
			</div>
		</div>
	</div>
	
<?php get_template_part('content','testimonials'); ?>

<?php
get_footer();
?>

</body>
</html>