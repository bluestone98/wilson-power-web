<?php
/**
* Template name: Buyers Persona
*/
get_header();?>
<!-- Timeline
================================================== -->
<div class="buyer-persona">
	<div class="row">
		<div class="timeline-start-top">
			<img src="<?php echo get_template_directory_uri(); ?>/images/Wilson-buyer-persona_-1_Cost-Efficiency-_03.png" alt="Movie">
		</div>
	</div>
	<section id="cd-timeline" class="cd-container">
		<div class="cd-timeline-block">
			<div class="cd-timeline-img">
				<img src="<?php echo get_template_directory_uri(); ?>/images/timeline-stops.png" alt="Movie">
				</div> <!-- cd-timeline-img -->
				<div class="cd-timeline-content header">
					<img class="text-center" src="<?php the_field('bipersona-image'); ?>" alt="" />
					<h1 class="timeline-heading"><?php the_field('bipersona-heading'); ?></h1>
					<?php if(the_field('bipersona-paragraph')):?>
					<p><?php the_field('bipersona-paragraph'); ?></p>
					<?php endif; ?>
					</div> <!-- cd-timeline-content -->
					</div> <!-- cd-timeline-block -->
					<?php
					// check if the repeater field has rows of data
					if( have_rows('repeater_timeline') ):
						// loop through the rows of data
					while ( have_rows('repeater_timeline') ) : the_row(); ?>
					<div class="cd-timeline-block">
						<div class="cd-timeline-img">
							<img src="<?php echo get_template_directory_uri(); ?>/images/timeline-stops.png" alt="Movie">
							</div> <!-- cd-timeline-img -->
							<div class="cd-timeline-content header time-line-box">
								<div class="cd-timeline-top"></div>
								<div class="cd-timeline-inner-content">
									
									<img src="<?php the_sub_field('main_timeline_image');?>" alt="" />
									
									<?php the_sub_field('main_timeline_content');?>
								</div>
								<div class="cd-timeline-bottom"></div>
								</div> <!-- cd-timeline-content -->
								</div> <!-- cd-timeline-block -->
								<?php	endwhile;
								else :
								// no rows found
								endif;
								?>
								</section> <!-- cd-timeline -->
							</div>
							<div class="row">
								<div class="timeline-start-bottom">
									<img src="<?php echo get_template_directory_uri(); ?>/images/Wilson-buyer-persona_-1_Cost-Efficiency-_03.png" alt="Movie">
								</div>
							</div>

			<?php get_template_part('content','testimonials'); ?>

			<div class="overlay-new overlay-about overlay-hugeinc black">
				
				<button type="button" class="overlay-close overlay-close-product">Close</button>
				
				<div class="outer-bg">
					<div class="inner-bg">
						<div class="form-block product">
							<div class="form-container">
								<iframe width="600" height="400" src="https://www.youtube.com/embed/TN1GfbKZUOs?autoplay=0" frameborder="0" allowfullscreen></iframe>
								<script type="text/javascript">
								</script>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<script type="text/javascript">
			(function() {
			var overlayProd = document.querySelector( 'div.overlay-about' );
			if( overlayProd  ) {
			var triggerBttnProd = document.getElementsByClassName( 'trigger-overlay-about' ),
			closeBttnProd = overlayProd.querySelector( 'button.overlay-close-product' );
			transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
			},
			transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
			support = { transitions : Modernizr.csstransitions };
			function toggleOverlay() {
			if( classie.has( overlayProd, 'open' ) ) {
			classie.remove( overlayProd, 'open' );
			classie.add( overlayProd, 'close' );
			var onEndTransitionFn = function( ev ) {
			if( support.transitions ) {
			if( ev.propertyName !== 'visibility' ) return;
			this.removeEventListener( transEndEventName, onEndTransitionFn );
			}
			classie.remove( overlayProd, 'close' );
			};
			if( support.transitions ) {
			overlayProd.addEventListener( transEndEventName, onEndTransitionFn );
			}
			else {
			onEndTransitionFn();
			}
			}
			else if( !classie.has( overlayProd, 'close' ) ) {
			classie.add( overlayProd, 'open' );
			}
			}
			for(var i=0;i<triggerBttnProd.length;i++){
				triggerBttnProd[i].addEventListener('click', toggleOverlay );
				}
				closeBttnProd.addEventListener( 'click', toggleOverlay );
				
				}
				
				})();
				</script>
				<?php
					get_footer();
				?>
				<script type="text/javascript">
				$('img.alignnone.trigger-overlay-about.size-medium.wp-image-1117').click(function (e) {
				if (e.target) {
				$('iframe').attr('src','https://www.youtube.com/embed/TN1GfbKZUOs?autoplay=1');
				}
				});
					$('button.overlay-close.overlay-close-product').click(function (e) {
				if (e.target) {
				$('iframe').attr('src','https://www.youtube.com/embed/TN1GfbKZUOs?autoplay=0');
				}
				});
				</script>
			</body>
		</html>