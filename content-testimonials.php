<!-- Section - 5
================================================== -->
<div class="section-5 <?php if (!is_page_template('home.php')) {echo 'grey-bg'; }?>">
	<div class="testimonials">
		<div class="row">
			<div class="large-12 columns">
			<div class="heading-txt">
				<h2>Testimonials</h2>
				<p></p><p>Don't just take our word for it ...</p>
				<p></p>
			</div>
				<div class="flexslider flexslider2">
					<ul class="slides">
						<?php
									$args = array(
										'post_type' => 'testimonial',
										'orderby'   => 'menu_order',
										'order'     => 'ASC'
									);
								$the_query = new WP_Query( $args );
								// The Loop
								if ( $the_query->have_posts() ) {
									while ( $the_query->have_posts() ) {
										$the_query->the_post();
										$image = get_field('testimonial_logo');
										if( !empty($image) ):
											$url = $image['url'];
											$title = $image['title'];
											$alt = $image['alt'];
									$image = "<figure><img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" /></figure>";
								else:
							$image = "<figure><img src=\"" . get_template_directory_uri() . "/images/photo-testi-01.png\" alt=\"PHOTO\" /></figure>";
						endif;
				?>
				<li>
					<aside>
						<?php echo $image; ?>
						<p><?php echo get_field('testimonial_content'); ?></p>
						<p class="cl-name"><?php echo get_field('testimonial_name'); ?></p>
						<p class="cl-desi"><?php echo get_field('testimonial_company'); ?></p>
					</aside>
				</li>
				<?php
						}
					}
				?>
			</ul>
		</div>
	</div>
</div>
</div>
</div>
<!-- Section - 6
================================================== -->
<div class="section-6">
<div class="row">
<div class="large-12 columns">
	<div id="owl-demo2" class="owl-carousel owl-theme">
		<?php
				$args = array(
					'post_type' => 'home_logos',
					'orderby'   => 'menu_order',
					'order'     => 'ASC'
				);
			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					$image = get_field('bottom_logo_image');
					if( !empty($image) ):
						$url = $image['url'];
						$title = $image['title'];
						$alt = $image['alt'];
						$size = 'product';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ];
				$image = "<figure><img src=\"" . $url ."\" alt=\"." . $alt . "\" /></figure>";
			else:
		$image = "<figure><img src=\"" . get_template_directory_uri() . "/images/logo-iso.gif\" alt=\"PHOTO\" /></figure>";
	endif;
?>
<div class="item"><?php echo $image; ?></div>
<?php
		}
	}
?>
</div>
</div>
</div>
<?php if($the_query->found_posts > 5): ?>
<div class="navigation">
<a class="previous prev2">Previous</a>
<a class="next next2">Next</a>
</div>
<?php endif; ?>
</div>