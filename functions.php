<?php

// Allow static page to be used as custom post type parent slug.

add_action( 'init', 'wpse16902_init' );
function wpse16902_init() {
    $GLOBALS['wp_rewrite']->use_verbose_page_rules = true;
}

add_filter( 'page_rewrite_rules', 'wpse16902_collect_page_rewrite_rules' );
function wpse16902_collect_page_rewrite_rules( $page_rewrite_rules )
{
    $GLOBALS['wpse16902_page_rewrite_rules'] = $page_rewrite_rules;
    return array();
}

add_filter( 'rewrite_rules_array', 'wspe16902_prepend_page_rewrite_rules' );
function wspe16902_prepend_page_rewrite_rules( $rewrite_rules )
{
    return $GLOBALS['wpse16902_page_rewrite_rules'] + $rewrite_rules;
}

add_action( 'init', 'create_post_type' );
add_theme_support( 'post-thumbnails' );
add_image_size( 'product', 275, 315 );
add_image_size( 'post-image', 348, 407 );

function create_post_type() {
  register_post_type( 'slider',
    array(
      'labels' => array(
        'name' => __( 'Sliders' ),
        'singular_name' => __( 'Slider' ),
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
  register_post_type( 'product',
    array(
      'labels' => array(
        'name' => __( 'Products' ),
        'singular_name' => __( 'Product' ),
      ),
      'public' => true,
      'has_archive' => true,
      'hierarchical'   => true,
      'supports'       => array( 'title','author', 'page-attributes' ),
      'taxonomies' => array('category'),
      'rewrite' => array('slug' => 'products'),
    )
  );
  register_post_type( 'partner',
    array(
      'labels' => array(
        'name' => __( 'Partners' ),
        'singular_name' => __( 'Partner' ),
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
  register_post_type( 'testimonial',
    array(
      'labels' => array(
        'name' => __( 'Testimonials' ),
        'singular_name' => __( 'Testimonial' ),
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
  register_post_type( 'home_logos',
    array(
      'labels' => array(
        'name' => __( 'Home Logos' ),
        'singular_name' => __( 'Home Logo' ),
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
  register_post_type( 'product_enquiry',
    array(
      'labels' => array(
        'name' => __( 'Product Enquiries' ),
        'singular_name' => __( 'Product Enquiry' ),
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
  register_post_type( 'contact_form_enquiry',
    array(
      'labels' => array(
        'name' => __( 'Contact Form Enquiries' ),
        'singular_name' => __( 'Contact Form Enquiry' ),
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
  register_post_type( 'customer_service',
    array(
      'labels' => array(
        'name' => __( 'Customer Service' ),
        'singular_name' => __( 'Customer Service' ),
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
  register_post_type( 'technical_support',
    array(
      'labels' => array(
        'name' => __( 'Technical Support' ),
        'singular_name' => __( 'Technical Support' ),
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
  register_post_type( 'case-studies',
    array(
      'labels' => array(
        'name' => __( 'Case-Studies' ),
        'singular_name' => __( 'Case-Study' ),
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'case-study','with_front' => false)
    )
  );
  //flush_rewrite_rules();
}

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
      'menu_title'  => 'Site Settings'
    ));
}

add_action( 'wp_ajax_product_enquiry', 'bs_sample_handler' );
add_action( 'wp_ajax_nopriv_product_enquiry', 'bs_sample_handler' );

function bs_sample_handler() {

    $post_data = array(
       'post_title' => "Enquiry from " . $_POST['first_name'] . " " . $_POST['last_name'],
       'post_content' => '',
       'post_status' => 'publish',
       'post_type' => 'product_enquiry',
      );

    // handle your form data here by accessing $_POST

    // handle your form data here by accessing $_POST
    $post_id = wp_insert_post( $post_data );

    update_field( 'pe_first_name', $_POST['first_name'], $post_id );
    update_field( 'pe_last_name', $_POST['last_name'], $post_id );
    update_field( 'pe_phone', $_POST['telephone'], $post_id );
    update_field( 'pe_email', $_POST['email'], $post_id );
    update_field( 'pe_company', $_POST['company'], $post_id );
    update_field( 'pe_enquiry', $_POST['enquiry'], $post_id );
    update_field( 'pe_product_name', $_POST['product_name'], $post_id );
    update_field( 'pe_product_url', $_POST['product_url'], $post_id );

    include('_postmark.php');

    /*
      Customer Confirmation
    */

    $product_name = $_POST['product_name'];

    $message = "<p>";


      $message .= "<strong>Name:</strong> " . $_POST['first_name'] . " " . $_POST['last_name'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Telephone:</strong> " . $_POST['telephone'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Email:</strong> " . $_POST['email'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Company:</strong> " . $_POST['company'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Enquiry:</strong> " . $_POST['enquiry'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Product Name:</strong> " . $_POST['product_name'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Product URL:</strong> " . $_POST['product_url'] . "&lt;EOL&gt;<br />";

    $message .= "</p>";

     $postmark = new Postmark( "02b09dbd-50d0-430c-a0e2-797c952e7601", "kelvin@bluestone98.com");
    $result = $postmark->to("webenquiries@wilsonpowersolutions.co.uk,lore.grohmann@wilsonpowersolutions.co.uk")
      ->subject( "New $product_name lead" )
      ->html_message( $message )
      ->send();


    // send some information back to the javascipt handler
    $response = array(
        'status' => '200',
        'message' => 'OK',
        'new_post_ID' => $post_id
    );

    // normally, the script expects a json respone
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode($response);

    exit;

}

add_action( 'wp_ajax_contact_enquiry', 'bs_contact_handler' );
add_action( 'wp_ajax_nopriv_contact_enquiry', 'bs_contact_handler' );

function bs_contact_handler() {

    $post_data = array(
       'post_title' => "Contact enquiry from " . $_POST['name'],
       'post_content' => '',
       'post_status' => 'publish',
       'post_type' => 'contact_form_enquiry',
      );

    // handle your form data here by accessing $_POST
    $post_id = wp_insert_post( $post_data );

    update_field( 'name', $_POST['name'], $post_id );
    update_field( 'phone', $_POST['telephone'], $post_id );
    update_field( 'email', $_POST['email'], $post_id );
    update_field( 'company', $_POST['company'], $post_id );
    update_field( 'enquiry', $_POST['enquiry'], $post_id );


    include('_postmark.php');

    /*
      Customer Confirmation
    */

    $message = "<p>";

      $message .= "<strong>Name:</strong> " . $_POST['name'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Telephone:</strong> " . $_POST['telephone'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Email:</strong> " . $_POST['email'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Company:</strong> " . $_POST['company'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Enquiry:</strong> " . $_POST['enquiry'] . "&lt;EOL&gt;<br />";

    $message .= "</p>";

    $postmarknew = new Postmark( "02b09dbd-50d0-430c-a0e2-797c952e7601", "andrew.leonard@bluestone98.com");
    $result = $postmarknew->to("kelvin@bluestone98.com","webenquiries@wilsonpowersolutions.co.uk","jpowley@itatspectrum.co.uk")
      ->subject( "New Contact Enquiry lead" )
      ->html_message( $message )
      ->send();



       // send some information back to the javascipt handler

    $response = array(
        'status' => '200',
        'message' => 'OK',
        'new_post_ID' => $post_id
    );

    // normally, the script expects a json respone
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode($response);

    exit;
}


add_action( 'wp_ajax_customer_service_enquiry', 'bs_customer_service_handler' );
add_action( 'wp_ajax_nopriv_customer_service_enquiry', 'bs_customer_service_handler' );

function bs_customer_service_handler() {

    $post_data = array(
       'post_title' => "Customer Service enquiry from " . $_POST['first_name'],
       'post_content' => '',
       'post_status' => 'publish',
       'post_type' => 'customer_service',
      );

    // handle your form data here by accessing $_POST
    $post_id = wp_insert_post( $post_data );

    update_field( 'forename*', $_POST['first_name'], $post_id );
    update_field( 'surname*', $_POST['last_name'], $post_id );
    update_field( 'email*', $_POST['email'], $post_id );
    update_field( 'company_name*', $_POST['company'], $post_id );
    update_field( 'enquiry', $_POST['enquiry'], $post_id );
    update_field( 'job_title', $_POST['jobtitle'], $post_id );
    update_field( 'main_telephone', $_POST['maintelephone'], $post_id );
    update_field( 'direct_dial', $_POST['directdial'], $post_id );
    update_field( 'mobile', $_POST['mobile'], $post_id );
    update_field( 'address_line_1', $_POST['addressline1'], $post_id );
    update_field( 'address_line_2', $_POST['addressline2'], $post_id );
    update_field( 'address_line_3', $_POST['addressline3'], $post_id );
    update_field( 'city', $_POST['city'], $post_id );
    update_field( 'country', $_POST['country'], $post_id );
    update_field( 'post_code', $_POST['postcode'], $post_id );



    include('_postmark.php');

    /*
      Customer Confirmation
    */

    $message = "<p>";

      $message .= "<strong>Forename:</strong> " . $_POST['first_name'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Surname:</strong> " . $_POST['last_name'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Email:</strong> " . $_POST['email'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Company:</strong> " . $_POST['company'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Enquiry:</strong> " . $_POST['enquiry'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Job Title:</strong> " . $_POST['jobtitle'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Main Telephone:</strong> " . $_POST['maintelephone'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Direct Dial:</strong> " . $_POST['directdial'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Mobile:</strong> " . $_POST['mobile'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Address Line 1:</strong> " . $_POST['addressline1'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Address Line 2:</strong> " . $_POST['addressline2'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Address Line 3:</strong> " . $_POST['addressline3'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>City:</strong> " . $_POST['city'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Country:</strong> " . $_POST['country'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Postcode:</strong> " . $_POST['postcode'] . "&lt;EOL&gt;<br />";

    $message .= "</p>";

    $postmarknew = new Postmark( "02b09dbd-50d0-430c-a0e2-797c952e7601", "kelvin@bluestone98.com");
    $result = $postmarknew->to(  "kelvin@bluestone98.com","webenquiries@wilsonpowersolutions.co.uk","jpowley@itatspectrum.co.uk" )
      ->subject( "New Customer Support Enquiry" )
      ->html_message( $message )
      ->send();



       // send some information back to the javascipt handler

    $response = array(
        'status' => '200',
        'message' => 'OK',
        'new_post_ID' => $post_id
    );

    // normally, the script expects a json respone
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode($response);

    exit;
}

add_action( 'wp_ajax_technical_support_enquiry', 'bs_technical_support_handler' );
add_action( 'wp_ajax_nopriv_technical_support_enquiry', 'bs_technical_support_handler' );

function bs_technical_support_handler() {

    $post_data = array(
       'post_title' => "Technical Support enquiry from " . $_POST['first_name'],
       'post_content' => '',
       'post_status' => 'publish',
       'post_type' => 'technical_support',
      );

    // handle your form data here by accessing $_POST
    $post_id = wp_insert_post( $post_data );

    update_field( 'forename*', $_POST['first_name'], $post_id );
    update_field( 'surname*', $_POST['last_name'], $post_id );
    update_field( 'email*', $_POST['email'], $post_id );
    update_field( 'company_name*', $_POST['company'], $post_id );
    update_field( 'enquiry', $_POST['enquiry'], $post_id );
    update_field( 'job_title', $_POST['jobtitle'], $post_id );
    update_field( 'main_telephone', $_POST['maintelephone'], $post_id );
    update_field( 'direct_dial', $_POST['directdial'], $post_id );
    update_field( 'mobile', $_POST['mobile'], $post_id );
    update_field( 'address_line_1', $_POST['addressline1'], $post_id );
    update_field( 'address_line_2', $_POST['addressline2'], $post_id );
    update_field( 'address_line_3', $_POST['addressline3'], $post_id );
    update_field( 'city', $_POST['city'], $post_id );
    update_field( 'country', $_POST['country'], $post_id );
    update_field( 'post_code', $_POST['postcode'], $post_id );



    include('_postmark.php');

    /*
      Customer Confirmation
    */

    $message = "<p>";

      $message .= "<strong>Forename:</strong> " . $_POST['first_name'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Surname:</strong> " . $_POST['last_name'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Email:</strong> " . $_POST['email'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Company:</strong> " . $_POST['company'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Enquiry:</strong> " . $_POST['enquiry'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Job Title:</strong> " . $_POST['jobtitle'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Main Telephone:</strong> " . $_POST['maintelephone'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Direct Dial:</strong> " . $_POST['directdial'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Mobile:</strong> " . $_POST['mobile'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Address Line 1:</strong> " . $_POST['addressline1'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Address Line 2:</strong> " . $_POST['addressline2'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Address Line 3:</strong> " . $_POST['addressline3'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>City:</strong> " . $_POST['city'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Country:</strong> " . $_POST['country'] . "&lt;EOL&gt;<br />";
      $message .= "<strong>Postcode:</strong> " . $_POST['postcode'] . "&lt;EOL&gt;<br />";

    $message .= "</p>";

    $postmarknew = new Postmark( "02b09dbd-50d0-430c-a0e2-797c952e7601", "kelvin@bluestone98.com");
    $result = $postmarknew->to(  "kelvin@bluestone98.com","sfewster@itatspectrum.co.uk","webenquiries@wilsonpowersolutions.co.uk" )
      ->subject( "New Technical Support Enquiry" )
      ->html_message( $message )
      ->send();



       // send some information back to the javascipt handler

    $response = array(
        'status' => '200',
        'message' => 'OK',
        'new_post_ID' => $post_id
    );

    // normally, the script expects a json respone
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode($response);

    exit;
}


function custom_field_excerpt() {
  global $post;
  $text =  get_sub_field('sub_field_content');;
  if ( '' != $text ) {
    $text = strip_shortcodes( $text );
    $text = apply_filters('the_content', $text);
    $text = str_replace(']]>', ']]>', $text);
    $excerpt_length = 2; // 20 words
    $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
    $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
  }
  return apply_filters('the_excerpt', $text);
}

function language_selector_flags(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        foreach($languages as $l){
            if(!$l['active']) echo '<a href="'.$l['url'].'">';
            echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
            if(!$l['active']) echo '</a>';
        }
    }
}