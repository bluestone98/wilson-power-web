<?php
/**
* Template name: Support
*/
get_header();?>
<section class="your-sector">
  <!-- Banner Inner
  ================================================== -->
  <div class="banner-innerout">
    <div class="banner-inner">
      <div class="row">
        <div class="medium-6 columns">
          <img class="hero-image" src="<?php the_field('intro-image'); ?>" alt=""/>
        </div>
        <div class="medium-6 columns">
          <aside>
            <p class="txt"><?php the_title();?></p>
            <?php the_field('your-sector-intro-text'); ?>
          </aside>
        </div>
      </div>
    </div>
  </div>
  <!-- Section - Three cols
  ================================================== -->
  <div class="section-col">
    <div class="row">
      <div class="article-cols">
        <?php
        $counter = 0;
        // check if the repeater field has rows of data
        if( have_rows('different-sectors') ):
        // loop through the rows of data
        while ( have_rows('different-sectors') ) : the_row(); ?>
        <div class="large-4 columns">
          <article>
            <h2 class="support-header"><?php the_sub_field('sub_field_header'); ?></h2>
          <img class="service-image" src="<?php the_sub_field('sub_field_image'); ?>" /></figure>
          <div class="content support">
            <div class="small-12 medium-12 large-12 columns">
              
              <?php the_sub_field('sub_field_content'); ?>
              
            </div>
            <div class="small-12 medium-12 large-12 columns">
              
              <a href="#" class="btn trigger-overlay-<?php echo $counter; ?>" ><span><?php   if($counter == 0){ echo 'Contact Customer Service'; }elseif($counter == 1){ echo 'Contact Technical Support'; }elseif($counter == 2){ echo 'Downloads'; } ?></span></a>
              
            </div>
          </div>
        </article>
      </div>
      <div class="overlay-new overlay-0 overlay-hugeinc black">
        
        <button type="button" class="overlay-close overlay-close-0">Close</button>
        
        <div class="outer-bg">
          <div class="inner-bg">
            <div class="form-block product customer-service">
              <div class="form-container">
                
                <div class="row customer">
                  <div class="medium-12 small-12 columns first">
                    
                    <div class="small-12 columns">
                      <div class="modal_header_intro">
                        <h2>Contact Customer Service</h2>
                        <h3>Wilson Power Solutions is committed to providing excellent customer service combined with performance and professionalism.</h3>
                        <h3>We will provide help and advice wherever we can or put you in touch with colleagues who will assist in addressing any issues you may raise. We endeavour to communicate courteously with customers by telephone, email, letter and face to face.</h3>
                        <h3>Please use the contact form below to get in touch.</h3>
                      </div>
                    </div>
                  </div>
                  
                  <div class="medium-12 columns second customer-service">
                    <div id="form-thank-customer-service-you">
                      <p>Thank you. We have received your request and will be in touch as soon as possible.</p>
                    </div>
                    <form action="/wp-admin/admin-ajax.php" id="customer-service-form" method="post">
                      <ul>
                        <li>
                          <input name="first_name" type="text" placeholder="Forename*" class="required" />
                        </li>
                        <li>
                          <input name="last_name" type="text" placeholder="Surname*" class="required" />
                        </li>
                        <li>
                          <input name="company" type="text" placeholder="Company Name*" class="required" />
                        </li>
                        <li>
                          <input name="email" type="email" placeholder="Email*" class="required" />
                        </li>
                        <li>
                          <textarea name="enquiry" placeholder="Contact Customer Services – How can we assist you? [please provide as much detail as possible]:*"></textarea>
                        </li>
                        <li>
                          <p>Additional Information</p>
                        </li>
                        <li>
                          <input name="jobtitle" type="text" placeholder="Job title" class="" />
                        </li>
                        <li>
                          <input name="maintelephone" type="text" placeholder="Main Telephone" class="" />
                        </li>
                        <li>
                          <input name="directdial" type="text" placeholder="Direct Dial" class="" />
                        </li>
                        <li>
                          <input name="mobile" type="text" placeholder="Mobile" class="" />
                        </li>
                        <li>
                          <input name="addressline1" type="text" placeholder="Address Line 1" class="" />
                        </li>
                        <li>
                          <input name="addressline2" type="text" placeholder="Address Line 2" class="" />
                        </li>
                        <li>
                          <input name="addressline3" type="text" placeholder="Address Line 3" class="" />
                        </li>
                        <li>
                          <input name="city" type="text" placeholder="City" class="" />
                        </li>
                        <li>
                          <input name="country" type="text" placeholder="Country" class="" />
                        </li>
                        <li>
                          <input name="postcode" type="text" placeholder="Post Code" class="" />
                        </li>
                        <li>
                          <input name="" type="submit" class="btn" value="Submit" />
                        </li>
                      </ul>
                    </form>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="overlay-new overlay-1 overlay-hugeinc black">
        
        <button type="button" class="overlay-close overlay-close-1">Close</button>
        
        <div class="outer-bg">
          <div class="inner-bg">
            <div class="form-block product customer-service">
              <div class="form-container">
                
                <div class="row customer">
                  <div class="medium-12 small-12 columns first">
                    
                    <div class="small-12 columns">
                      <div class="modal_header_intro">
                        <h2>Contact Technical Support</h2>
                        <h3>The Wilson Technical Support team is committed to supporting our customers at any time, trouble shooting where appropriate or escalating issues to relevant engineering teams.</h3>
                      </div>
                    </div>
                  </div>
                  
                  <div class="medium-12 columns second technical-support">
                    <div id="form-thank-technical-support-you">
                      <p>Thank you. We have received your request and will be in touch as soon as possible.</p>
                    </div>
                    <form action="/wp-admin/admin-ajax.php" id="technical-support-form" method="post">
                      <ul>
                        <li>
                          <input name="first_name" type="text" placeholder="Forename*" class="required" />
                        </li>
                        <li>
                          <input name="last_name" type="text" placeholder="Surname*" class="required" />
                        </li>
                        <li>
                          <input name="company" type="text" placeholder="Company Name*" class="required" />
                        </li>
                        <li>
                          <input name="email" type="email" placeholder="Email Address*" class="required" />
                        </li>
                        <li>
                          <textarea name="enquiry" placeholder="Technical Support Enquiry [please provide as much detail as possible]:*"></textarea>
                        </li>
                        <li>
                          <p>Additional Information</p>
                        </li>
                        <li>
                          <input name="jobtitle" type="text" placeholder="Job title" class="" />
                        </li>
                        <li>
                          <input name="maintelephone" type="text" placeholder="Main Telephone" class="" />
                        </li>
                        <li>
                          <input name="directdail" type="text" placeholder="Direct Dial" class="" />
                        </li>
                        <li>
                          <input name="mobile" type="text" placeholder="Mobile" class="" />
                        </li>
                        <li>
                          <input name="addressline1" type="text" placeholder="Address Line 1" class="" />
                        </li>
                        <li>
                          <input name="addressline2" type="text" placeholder="Address Line 2" class="" />
                        </li>
                        <li>
                          <input name="addressline3" type="text" placeholder="Address Line 3" class="" />
                        </li>
                        <li>
                          <input name="city" type="text" placeholder="City" class="" />
                        </li>
                        <li>
                          <input name="country" type="text" placeholder="Country" class="" />
                        </li>
                        <li>
                          <input name="postcode" type="text" placeholder="Post Code" class="" />
                        </li>
                        <li>
                          <input name="" type="submit" class="btn" value="Submit" />
                        </li>
                      </ul>
                    </form>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="overlay-new overlay-2 overlay-hugeinc black">
        
        <button type="button" class="overlay-close overlay-close-2">Close</button>
        
        <div class="outer-bg">
          <div class="inner-bg">
            <div class="form-block product customer-service">
              <div class="form-container">
                
                <div class="row">
                  <div class="medium-12 small-12 columns first">
                    
                    <div class="small-12 columns">
                      <div class="modal_header_intro">
                        <h2>Downloads</h2>
                      </div>
                    </div>
                    <div class="small-4 columns">
                      <img src="<?php the_sub_field('sub_field_content_icon'); ?>" />
                    </div>
                  </div>
                  
                  <div class="medium-12 columns second customer-service">
                    <h1>Products</h1>
                                     <?php if(get_field('product_brochures')): ?>
        <ul class="large-block-grid-2">
          <?php while(has_sub_field('product_brochures')): ?>
                <?php 

                  $file = get_sub_field('product_files');

                  if( $file ): 

                    // vars
                    $url = $file['url'];
                    $title = $file['title'];
                    $caption = $file['caption']; ?>

                  <li>
                      <img src="<?php echo get_template_directory_uri(); ?>/images/download.png" alt="downloads">
                      <p><a href="<?php echo $url; ?>" target="_blank"><?php echo $title; ?></a></p>
                  <li><!-- Your content goes here -->

              <?php endif; ?>
                <?php endwhile; ?>
     </ul>
   <?php endif; ?>
              <h1>Reference</h1>
                      <?php if(get_field('reference_brochures')): ?>
        <ul class="large-block-grid-2">
          <?php while(has_sub_field('reference_brochures')): ?>
                <?php 

                  $file = get_sub_field('reference_files');

                  if( $file ): 

                    // vars
                    $url = $file['url'];
                    $title = $file['title'];
                    $caption = $file['caption']; ?>

                  <li>
                      <img src="<?php echo get_template_directory_uri(); ?>/images/download.png" alt="downloads">
                      <p><a href="<?php echo $url; ?>" target="_blank"><?php echo $title; ?></a></p>
                  <li><!-- Your content goes here -->

              <?php endif; ?>
                <?php endwhile; ?>
     </ul>
   <?php endif; ?>
          
          
      


        <h1>Our Company</h1>
        <?php if(get_field('company_brochures')): ?>
        <ul class="large-block-grid-2">
          <?php while(has_sub_field('company_brochures')): ?>
                <?php 

                  $file = get_sub_field('company_files');

                  if( $file ): 

                    // vars
                    $url = $file['url'];
                    $title = $file['title'];
                    $caption = $file['caption']; ?>

                  <li>
                      <img src="<?php echo get_template_directory_uri(); ?>/images/download.png" alt="downloads">
                      <p><a href="<?php echo $url; ?>" target="_blank"><?php echo $title; ?></a></p>
                  <li><!-- Your content goes here -->

              <?php endif; ?>
                <?php endwhile; ?>

          
          
       </ul>
   <?php endif; ?>

</div>
</div>
</div>
</div>
</div>
</div>

</div>
<script type="text/javascript">
(function() {
var overlayProd = document.querySelector( 'div.overlay-<?php echo $counter; ?>' );
if( overlayProd  ) {
var triggerBttnProd = document.getElementsByClassName( 'trigger-overlay-<?php echo $counter; ?>' ),
closeBttnProd = overlayProd.querySelector( 'button.overlay-close-<?php echo $counter; ?>' );
transEndEventNames = {
'WebkitTransition': 'webkitTransitionEnd',
'MozTransition': 'transitionend',
'OTransition': 'oTransitionEnd',
'msTransition': 'MSTransitionEnd',
'transition': 'transitionend'
},
transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
support = { transitions : Modernizr.csstransitions };
function toggleOverlay() {
if( classie.has( overlayProd, 'open' ) ) {
classie.remove( overlayProd, 'open' );
classie.add( overlayProd, 'close' );
var onEndTransitionFn = function( ev ) {
if( support.transitions ) {
if( ev.propertyName !== 'visibility' ) return;
this.removeEventListener( transEndEventName, onEndTransitionFn );
}
classie.remove( overlayProd, 'close' );
};
if( support.transitions ) {
overlayProd.addEventListener( transEndEventName, onEndTransitionFn );
}
else {
onEndTransitionFn();
}
}
else if( !classie.has( overlayProd, 'close' ) ) {
classie.add( overlayProd, 'open' );
}
}
for(var i=0;i<triggerBttnProd.length;i++){
triggerBttnProd[i].addEventListener('click', toggleOverlay );
}
closeBttnProd.addEventListener( 'click', toggleOverlay );
}

})();
</script>
<?php $counter++; ?>
<?php
endwhile;
else :
// no rows found
endif;
?>

</div>
</div>
</div>
<?php get_template_part('content','testimonials'); ?>
<?php
get_footer();
?>
<script type="text/javascript">
$(function(){
$("#customer-service-form").validate({
errorElement: 'span',
submitHandler: function(form) {
var _data = {  'nonce' : '<?php echo wp_create_nonce( "ajax_post_author_credit_nonce" ); ?>', 'action' : 'customer_service_enquiry' }
$(form).ajaxSubmit({
dataType: 'json',
data: _data,
beforeSubmit: function(arr, $form, options) {
var first_name = $('#customer-service-form input[name="first_name"]').fieldValue();
var last_name = $('#customer-service-form input[name="last_name"]').fieldValue();
var email = $('#customer-service-form input[name="email"]').fieldValue();
var company = $('#customer-service-form input[name="company"]').fieldValue();
var enquiry = $('#customer-service-form textarea').fieldValue();
},
success: function(resp){
console.log(resp);
$('#customer-service-form').fadeOut(function(){
$('#form-thank-customer-service-you').fadeIn(function(){
setTimeout(function(){
$('.overlay-close.overlay-close-0').click();
}, 2000);
});
});
},
error: function(resp){
console.log(resp);
alert('Sorry there was an error with your submission. Please try again later.');
}
});
}
});
$("#technical-support-form").validate({
errorElement: 'span',
submitHandler: function(form) {
var _data = {  'nonce' : '<?php echo wp_create_nonce( "ajax_post_author_credit_nonce" ); ?>', 'action' : 'technical_support_enquiry' }
$(form).ajaxSubmit({
dataType: 'json',
data: _data,
beforeSubmit: function(arr, $form, options) {
var first_name = $('#technical-support-form input[name="first_name"]').fieldValue();
var last_name = $('#technical-support-form input[name="last_name"]').fieldValue();
var email = $('#technical-support-form input[name="email"]').fieldValue();
var company = $('#technical-support-form input[name="company"]').fieldValue();
var enquiry = $('#technical-support-form textarea').fieldValue();
},
success: function(resp){
console.log(resp);
$('#technical-support-form').fadeOut(function(){
$('#form-thank-technical-support-you').fadeIn(function(){
setTimeout(function(){
$('.overlay-close.overlay-close-1').click();
}, 2000);
});
});
},
error: function(resp){
console.log(resp);
alert('Sorry there was an error with your submission. Please try again later.');
}
});
}
});
});
</script>
</body>
</html>