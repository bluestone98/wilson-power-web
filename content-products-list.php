 <!-- Section - Products
     ================================================== -->
     <div class="section-1" id="all-products">
     <div class="row">
     <div class="large-12 columns">

          <div class="product-list">

          <?php

            global $post;
            $args = array(
                'post_type' => 'product',
                'orderby'   => 'menu_order',
                'order'     => 'ASC',
                'cat'      => 20,
              );

            $the_query = new WP_Query( $args );

            // The Loop
            if ( $the_query->have_posts() ) {

              while ( $the_query->have_posts() ) {
                $the_query->the_post();

                if( have_rows('product_page_content') ):

                  while ( have_rows('product_page_content') ) : the_row();

                  if( get_row_layout() == "product_information"):

                    $image = get_sub_field('home_page_product_image');

                    if( !empty($image) ):
                      $url = $image['url'];
                      $title = $image['title'];
                      $alt = $image['alt'];
                      $size = 'product';
                      $thumb = $image['sizes'][ $size ];
                      $width = $image['sizes'][ $size . '-width' ];
                      $height = $image['sizes'][ $size . '-height' ];

                      $image = "<figure><img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" width=\"." . $width . "\" height=\"." . $height . "\"/></figure>";
                    else:
                      $image = "<figure><img src=\"" . get_template_directory_uri() . "/images/product-wilson-01.jpg\" alt=\"PHOTO\" /></figure>";
                    endif;

            ?>


            <article>
             <?php echo $image; ?>
               <aside>
                 <div class="cont">
                 <h3><?php the_title(); ?></h3>
                 <a href="<?php echo get_the_permalink(); ?>" class="btn">Read More</a>
                 </div>
               </aside>
            </article>

          <?php
                endif;
                endwhile;

                endif;

              }
            }

          ?>


          </div>

     </div>
     </div>
     </div>