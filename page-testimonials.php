<?php
/**
* Template name: testimonial
*/
get_header();?>
<section class="your-sector testimonial">
	<!-- Banner Inner
	================================================== -->
	<div class="banner-innerout">
		<div class="banner-inner">
			<div class="row">
				<div class="medium-6 columns">
					<img class="hero-image" src="<?php the_field('intro-image'); ?>" alt=""/>
				</div>
				<div class="medium-6 columns">
					<aside>
						<p class="txt"><?php the_title();?></p>
						<?php the_field('your-sector-intro-text'); ?>
					</aside>
				</div>
			</div>
		</div>
	</div>
	<!-- Section - Three cols
	================================================== -->
	<?php
				$args = array(
					'post_type' => 'testimonial',
					'orderby'   => 'menu_order',
					'order'     => 'ASC'
				);
			$the_query = new WP_Query( $args );
			// The Loop
			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
?>
	<div class="section-col">
			<div class="row">
				<div class="article-cols">
					<div class="large-12 columns">
						<article class="services">

							<div class="small-12 medium-4 large-2 columns first ">
								<?php 	$image = get_field('testimonial_logo');
											if( !empty($image) ):
												$url = $image['url'];
												$title = $image['title'];
												$alt = $image['alt'];
														$image = "<figure><img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" /></figure>";
													else:
												$image = "<figure><img src=\"" . get_template_directory_uri() . "/images/photo-testi-01.png\" alt=\"PHOTO\" /></figure>";
											endif;
									?>
									<?php echo $image; ?>
							</div>
							<div class="small-12 medium-8 large-10 columns second">
								<p><?php echo get_field('testimonial_content'); ?></p>
								<p class="cl-name"><strong><?php echo get_field('testimonial_name'); ?><?php echo get_field('testimonial_company'); ?></strong></p>
								<p class="cl-desi"><strong><?php echo get_field('testimonial_company'); ?></strong></p>
							</div>

					</article>
				</div>

			</div>
		</div>
	</div>
	<?php
			}
		}
	?>

<?php
get_footer();
?>


<script type="text/javascript">

	$('.your-sector .section-col:even').find('div.small-12.medium-4.large-2.columns.first').attr('class','small-12 medium-4 large-2 large-push-10 columns');
	$('.your-sector .section-col:even').find('div.small-12.medium-8.large-10.columns.second').attr('class','small-12 medium-8 large-10 large-pull-2 columns');

</script>
</body>
</html>
