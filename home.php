<?php
 /**
 * Template name: Blog
 */

	get_header();?>


  <section class="blog">
<article class="row">
 <div class="grid-sizer"></div>
 <div class="grid">

   <?php        $args = array(
            'post_type' => 'post',
          );
   $latest= new WP_Query( $args );
   if($latest->have_posts()): ?>
       <?php while( $latest->have_posts() ) : $latest->the_post(); ?>
   <div class="grid-item">
     <div class="cd-timeline-top"></div>
     <div class="grid-item-content">
       <?php if ( has_post_thumbnail() ) { ?>
     	<a href="<<?php echo get_permalink(); ?>" ><?php the_post_thumbnail('post-image'); ?></a>
       <?php	}  ?>
   <div class="grid-item-content-inner">
     <?php the_title('<h1>','</h1>');?>
   <p><?php the_excerpt(); ?></p>
   <a href="<?php echo get_permalink(); ?>"><button> Read More</button></a>
    </div>
  </div>
     <div class="grid-item-content--bottom"><img src="<?php echo get_template_directory_uri(); ?>/images/calendar.png" alt="Movie">&emsp;<?php echo mysql2date('j M Y', $post->post_date); ?></div>
   </div>
   <?php endwhile; ?>
 <?php endif; ?>

 </div>
  </article>


<!-- Section - 5
================================================== -->


</section>
<?php get_template_part('content','testimonials'); ?>
<?php
get_footer();
?>


</body>
</html>
