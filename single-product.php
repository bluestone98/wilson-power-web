<?php
  get_header();


    if( have_rows('product_page_content') ):

      while ( have_rows('product_page_content') ) : the_row();

        $image = get_sub_field('product_image');

        if( !empty($image) ):

          $url = $image['url'];
          $title = $image['title'];
          $alt = $image['alt'];

          $image = "<figure><img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" /></figure>";
        else:
          $image = "<figure><img src=\"" . get_template_directory_uri() . "/images/product-wilson-01.jpg\" alt=\"PHOTO\" /></figure>";
        endif;


       $the_product = get_the_title();

?>

<?php if(get_row_layout() == "product_information"): ?>

     <!-- Banner Inner
     ================================================== -->
     <div class="banner-innerout">
     <div class="banner-inner">
     <div class="row">

          <div class="medium-6 columns">
            <?php echo $image; ?>
          </div>


          <div class="medium-6 columns">
          <aside>
          <p class="txt">Products  /</p>

          <h2><?php the_title(); ?></h2>

          <p><?php echo get_sub_field('product_description'); ?></p>

          <div class="btn-out">

              <a href="#" class="btn trigger-overlay-product"><span>Make a product enquiry</span></a>


              <a href="#" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="btn right trigger-overlay-download"><span>Download product info</span></a>
              <?php if(get_field('product_info_link')): ?>
              <ul id="drop1" data-dropdown-content class="f-dropdown" aria-hidden="true">

              <?php while(has_sub_field('product_info_link')): ?>
                <?php 

                  $file = get_sub_field('product_info_link_to_file');

                  if( $file ): 

                    // vars
                    $url = $file['url'];
                    $title = $file['title'];
                    $caption = $file['caption']; ?>
                <li><a href="<?php echo $url; ?>" target="_blank"><?php echo $title; ?></a></li>
              <?php endif; ?>
                <?php endwhile; ?>

              </ul>
              <?php endif; ?>
          </div>
          </aside>
          </div>

     </div>
     </div>
     </div>

<?php endif; // product information ?>


<?php if(get_row_layout() == "two_column_block_with_images"): ?>


    <!-- Section - Two cols
     ================================================== -->
     <div class="section-col">
     <div class="row">

          <div class="article-cols">
          <div class="medium-6 columns">
            <article>
            <h2><?php echo get_sub_field('tcb_left_title'); ?></h2>

            <p><span><?php echo get_sub_field('tcb_left_intro'); ?></span></p>

            <figure><img src="<?php echo get_sub_field('tcb_left_image'); ?>" /></figure>

            <p><?php echo get_sub_field('tcb_left_main_paragraph'); ?></p>
            </article>
          </div>


          <div class="medium-6 columns">
           <article>
            <h2><?php echo get_sub_field('tcb_right_title'); ?></h2>

            <p><span><?php echo get_sub_field('tcb_right_intro'); ?></span></p>

            <figure><img src="<?php echo get_sub_field('tcb_right_image'); ?>" /></figure>

            <p><?php echo get_sub_field('tcb_right_main_paragraph'); ?></p>
          </article>
          </div>
          </div>

     </div>
     </div>


<?php endif; // two_column_block_with_images ?>



<?php if(get_row_layout() == "two_column_block_text_left_image_right"): ?>


 <!-- Section - Two cols Lv2
 ================================================== -->
 <div class="section-col col-lv2">
 <div class="row">

      <div class="article-cols">
      <div class="medium-6 columns">
      <article>

      <h2><?php echo get_sub_field('tcbtlir_title'); ?></h2>

      <p><?php echo get_sub_field('tcbtlir_paragraph'); ?></p>

      <?php if(get_sub_field('tcbtlir_include_button')): ?>
        <a href="<?php echo get_sub_field('tcbtlir_button_links_to'); ?>" class="btn"><?php echo get_sub_field('tcbtlir_button_label'); ?></a>
      <?php endif; ?>

      </article>
      </div>


      <div class="medium-6 columns">
      <article>
        <figure><img src="<?php echo get_sub_field('tcbtlir_image'); ?>" /></figure>
      </article>
      </div>
      </div>

 </div>
 </div>



<?php endif; // two_column_block_text_left_image_right ?>


<?php if(get_row_layout() == "two_column_block_text_right_image_left"): ?>


 <!-- Section - Two cols Lv2
     ================================================== -->
     <div class="section-col col-lv2">
     <div class="row">

          <div class="article-cols">
          <div class="medium-6 columns">
          <article>
          <figure><img src="<?php echo get_sub_field('tcbtril_image'); ?>" /></figure>
          </article>
          </div>


          <div class="medium-6 columns">

            <article>

              <h2><?php echo get_sub_field('tcbtril_title'); ?></h2>
              <p><?php echo get_sub_field('tcbtril_paragraph'); ?></p>

              <?php if(get_sub_field('tcbtril_include_button')): ?>
              <a href="<?php echo get_sub_field('tcbtril_button_links_to'); ?>" class="btn" target="_blank"><?php echo get_sub_field('tcbtril_button_label'); ?></a>
              <?php endif; ?>

            </article>

          </div>
          </div>

     </div>
     </div>


<?php endif; // two_column_block_text_right_image_left ?>


<?php if(get_row_layout() == "three_column"): ?>

 <!-- Section - Three cols
     ================================================== -->
     <div class="section-col col-three">
     <div class="row">

          <div class="article-cols">

          <div class="medium-4 columns">
          <article>
          <h2><?php echo get_sub_field('tc_title_1'); ?></h2>

          <p><span><?php echo get_sub_field('tc_intro_1'); ?></span></p>

          <figure><img src="<?php echo get_sub_field('tc_image_1'); ?>" /></figure>

          <p><?php echo get_sub_field('tc_paragraph_1'); ?></p>
          </article>
          </div>


          <div class="medium-4 columns">
          <article>
          <h2><?php echo get_sub_field('tc_title_2'); ?></h2>

          <p><span><?php echo get_sub_field('tc_intro_2'); ?></span></p>

          <figure><img src="<?php echo get_sub_field('tc_image_2'); ?>" /></figure>

          <p><?php echo get_sub_field('tc_paragraph_2'); ?></p>
          </article>
          </div>


          <div class="medium-4 columns">
          <article>
          <h2><?php echo get_sub_field('tc_title_3'); ?></h2>

          <p><span><?php echo get_sub_field('tc_intro_3'); ?></span></p>

          <figure><img src="<?php echo get_sub_field('tc_image_3'); ?>" /></figure>

          <p><?php echo get_sub_field('tc_paragraph_3'); ?></p>
          </article>
          </div>
          </div>

     </div>
     </div>

<?php endif; // three column ?>


<?php if(get_row_layout() == "four_column"): ?>

 <!-- Section - Four cols
     ================================================== -->
     <div class="section-col col-three">
     <div class="row">

          <div class="article-cols">
          <div class="medium-3 columns">
          <article>
          <h2><?php echo get_sub_field('fc_title_1'); ?></h2>

          <p><span><?php echo get_sub_field('fc_intro_1'); ?></span></p>

          <figure><img src="<?php echo get_sub_field('fc_image_1'); ?>" /></figure>

          <p><?php echo get_sub_field('fc_paragraph_1'); ?></p>
          </article>
          </div>


          <div class="medium-3 columns">
          <article>
          <h2><?php echo get_sub_field('fc_title_2'); ?></h2>

          <p><span><?php echo get_sub_field('fc_intro_2'); ?></span></p>

          <figure><img src="<?php echo get_sub_field('fc_image_2'); ?>" /></figure>

          <p><?php echo get_sub_field('fc_paragraph_2'); ?></p>
          </article>
          </div>


          <div class="medium-3 columns">
          <article>
          <h2><?php echo get_sub_field('fc_title_3'); ?></h2>

          <p><span><?php echo get_sub_field('fc_intro_3'); ?></span></p>

          <figure><img src="<?php echo get_sub_field('fc_image_3'); ?>" /></figure>

          <p><?php echo get_sub_field('fc_paragraph_3'); ?></p>
          </article>
          </div>


          <div class="medium-3 columns">
          <article>
          <h2><?php echo get_sub_field('fc_title_4'); ?></h2>

          <p><span><?php echo get_sub_field('fc_intro_4'); ?></span></p>

          <figure><img src="<?php echo get_sub_field('fc_image_4'); ?>" /></figure>

          <p><?php echo get_sub_field('fc_paragraph_4'); ?></p>
          </article>
          </div>
          </div>

     </div>
     </div>



<?php endif; // three column ?>


<?php
  endwhile;
  endif;
?>

<?php $header =  get_field( "table_header" );

if (!empty($header)){ ?>
     <div class="section-col col-three">
     <div class="row">

          <div class="article-cols">
          <div class="medium-12 columns">
          <article>
          <h2 class="text-center"><?php echo $header; ?></h2>
            


<?php $table = get_field('table');
        if($table): ?>
        <?php echo '<table border="0">';

        if ( $table['header'] ) {

            echo '<thead>';

                echo '<tr>';

                    foreach ( $table['header'] as $th ) {

                        echo '<th>';
                            echo $th['c'];
                        echo '</th>';
                    }

                echo '</tr>';

            echo '</thead>';
        }

        echo '<tbody>';

            foreach ( $table['body'] as $tr ) {

                echo '<tr>';

                    foreach ( $tr as $td ) {

                        echo '<td>';
                            echo $td['c'];
                        echo '</td>';
                    }

                echo '</tr>';
            }

        echo '</tbody>';

    echo '</table>';?>
          
<?php endif; ?>
          </article>
          </div>

          </div>
          </div>

     </div>
     </div>
<?php } ?>

 

     <?php 

        $video = get_field('product_video');
        if($video): ?>
     <div class="section-col col-three">
     <div class="row">

          <div class="article-cols">
          <div class="medium-12 columns">
          <article>

  <div class="flex-video">
                <iframe width="420" height="315" src="<?php echo $video; ?>" frameborder="0" allowfullscreen></iframe>
          </div>

          

          </article>
          </div>

          </div>
          </div>

     </div>
     </div>
   <?php endif; ?>

<?php

  $pro = get_field('relate_to_products');

  if( $pro ): ?>
  <?php foreach( $pro as $post): // variable must be called $post (IMPORTANT) 
  ?>

  <?php setup_postdata($post); ?>
  <?php    if( have_rows('product_page_content') ):

      while ( have_rows('product_page_content') ) : the_row();

        $image = get_sub_field('product_image');

        if( !empty($image) ):

          $url = $image['url'];
          $title = $image['title'];
          $alt = $image['alt'];

          $image = "<figure><img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" /></figure>";
        else:
          $image = "<figure><img src=\"" . get_template_directory_uri() . "/images/product-wilson-01.jpg\" alt=\"PHOTO\" /></figure>";
        endif;


       $the_product = get_the_title();?>

         
           <?php if(get_row_layout() == "product_information"): ?>
    <div class="section-col col-three grey single product">
     <div class="row">



          <div class="article-cols">
          <div class="medium-6 columns">
       
            <?php echo $image; ?>

         
          </div>


          <div class="medium-6 columns">
          <aside>
         
          <h2><?php the_title(); ?></h2>

          <p><?php echo get_sub_field('product_description'); ?></p>
          <a href="<?php the_permalink(); ?>" class="btn trigger-overlay-0"><span>Read More</span></a>
           
          </aside>
          </div>
          </div>





          </div>
          </div>
          <?php endif;?>
           <?php endwhile;?>
      <?php endif;?>
          
<?php endforeach; ?>
 
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>



<?php

  $posts = get_field('relationship_field_name');

  if( $posts ): ?>
  <div class="section-5 grey-bg">
    <div class="row">
      <div class="large-12 columns">
        <div class="heading-txt">
          <h2>Testimonials</h2>
          <p></p><p>Don't just take our word for it ...</p>
          <p></p>
        </div>
      </div>
    </div>
    <div class="testimonials">
      <div class="row">
        <div class="large-12 columns">
          <div class="flexslider flexslider2">
      <ul class="slides">
      <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
          <?php setup_postdata($post); ?>
          <li>
            <?php $image = get_field('testimonial_logo');
            if( !empty($image) ):
            $url = $image['url'];
            $title = $image['title'];
            $alt = $image['alt'];
            $image = "<figure><img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" /></figure>";
            else:
            $image = "<figure><img src=\"" . get_template_directory_uri() . "/images/photo-testi-01.png\" alt=\"PHOTO\" /></figure>";
            endif;
            ?>
            <aside>
            <?php echo $image; ?>
              <p><?php echo get_field('testimonial_content'); ?></p>
              <p class="cl-name"><?php echo get_field('testimonial_name'); ?></p>
              <p class="cl-desi"><?php echo get_field('testimonial_company'); ?></p>
              </aside>
          </li>
      <?php endforeach; ?>
      </ul>
      <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
    </div>
  </div>
</div>
</div>
</div>
  <?php endif; ?>


<?php get_template_part('content','products-list'); ?>



<!-- Product Contact form
================================================== -->
<div class="overlay-new overlay-product overlay-hugeinc black">

     <button type="button" class="overlay-close overlay-close-product">Close</button>

     <div class="outer-bg">
     <div class="inner-bg">
     <div class="form-block product">
     <div class="form-container">

     <div id="form-thank-you">
       <p>Thank you. We have received your request and will be in touch as soon as possible.</p>
     </div>

     <form action="/wp-admin/admin-ajax.php" id="sample-request-form" method="post">
        <ul>
           <li>
            <p>Please leave us your details and we will get back to you as soon as we can.</p>
           </li>
           <li>
            <input name="first_name" type="text" placeholder="First Name" class="required" />
           </li>
           <li>
            <input name="last_name" type="text" placeholder="Last Name" class="required" />
           </li>
           <li>
            <input name="company" type="text" placeholder="Company" class="required" />
           </li>
           <li>
            <input name="email" type="email" placeholder="Email" class="required" />
           </li>
           <li>
            <input name="telephone" type="tel" placeholder="Phone" class="required" />
           </li>
         <li>
           <textarea name="enquiry" placeholder="Enquiry details (optional)"></textarea>
         </li>
           <li>
            <input name="" type="submit" class="btn" value="Submit" />
           </li>
         </ul>

         <input name="product_name" type="hidden" value="<?php echo $the_product; ?>" />
         <input name="product_url" type="hidden" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />

      </form>

     </div>
     </div>
     </div>
     </div>

</div>

   <div class="overlay-new overlay-2 overlay-hugeinc black">

      <button type="button" class="overlay-close overlay-close-download">Close</button>

        <div class="outer-bg">
          <div class="inner-bg">
            <div class="form-block product customer-service">
              <div class="form-container">

                <div class="row">
                  <div class="medium-12 small-12 columns first product-download-repeater">

                    <div class="small-12 columns">
                      <div class="modal_header_intro">
                        <h2>Downloads</h2>
                      </div>
                    </div>
                  <div class="medium-12 columns second product-download-repeater">
                    <ul class="large-block-grid-2">



                      <?php if( have_rows('product_brochure_download_file_repeater') ):
                      	// loop through the rows of data
                         while ( have_rows('product_brochure_download_file_repeater') ) : the_row();?>
                      <li><!-- Your content goes here -->
                      <img src="<?php echo get_template_directory_uri(); ?>/images/download.png" alt="downloads">
                      <p><a href="#" target="_blank"><?php the_sub_field('product_brochure_download_file_name'); ?></a></p>
                    </li>
                    <?php endwhile;
                      else :
                          // no rows found
                      endif;
                    ?>
              </ul>
            </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<?php
  get_footer();
?>

<script type="text/javascript">
(function() {
var overlayProd = document.querySelector( 'div.overlay-download' );
if( overlayProd  ) {
var triggerBttnProd = document.getElementsByClassName( 'trigger-overlay-download' ),
closeBttnProd = overlayProd.querySelector( 'button.overlay-close-2' );
transEndEventNames = {
'WebkitTransition': 'webkitTransitionEnd',
'MozTransition': 'transitionend',
'OTransition': 'oTransitionEnd',
'msTransition': 'MSTransitionEnd',
'transition': 'transitionend'
},
transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
support = { transitions : Modernizr.csstransitions };
function toggleOverlay() {
if( classie.has( overlayProd, 'open' ) ) {
classie.remove( overlayProd, 'open' );
classie.add( overlayProd, 'close' );
var onEndTransitionFn = function( ev ) {
if( support.transitions ) {
if( ev.propertyName !== 'visibility' ) return;
this.removeEventListener( transEndEventName, onEndTransitionFn );
}
classie.remove( overlayProd, 'close' );
};
if( support.transitions ) {
overlayProd.addEventListener( transEndEventName, onEndTransitionFn );
}
else {
onEndTransitionFn();
}
}
else if( !classie.has( overlayProd, 'close' ) ) {
classie.add( overlayProd, 'open' );
}
}
for(var i=0;i<triggerBttnProd.length;i++){
triggerBttnProd[i].addEventListener('click', toggleOverlay );
}
closeBttnProd.addEventListener( 'click', toggleOverlay );
}

})();
</script>
</body>
</html>