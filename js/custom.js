/* offcanvas */
$(document).ready(function () {
  $('[data-toggle=offcanvas]').click(function () {
    $('.row-offcanvas').toggleClass('active')
  });
  
  $('.sidebar-offcanvas .close').click(function () {
    $('.row-offcanvas').toggleClass('active')
  });

});


/* Sliders */
$(window).load(function() {
    
    $('.flexslider1').flexslider({
    animation: "slide",
    easing: "easeInElastic",
    slideshow: true,  
    slideshowSpeed: 5000,
    animationSpeed: 600,
    start: function(slider){
    $('body').removeClass('loading');
    }
    });
    
    $('.flexslider2').flexslider({
    animation: "slide",
    easing: "easeInElastic",
    slideshow: true,  
    slideshowSpeed: 5000,
    animationSpeed: 600,
    start: function(slider){
    $('body').removeClass('loading');
    }
    });
    
});


$(document).ready(function() {
    var owl = $("#owl-demo1");
    owl.owlCarousel({
    
    navigation : true,
    autoPlay : false,
    slideSpeed : 300,
    paginationSpeed : 400,
    singleItem : false,
    items : 6,
    itemsDesktop : [1170,6],
    itemsDesktopSmall : [1139,4],
    itemsTablet : [1023,3],
    itemsTabletSmall : [767,2],
    itemsMobile : [479,1],
    loop : true,
    //autowidth : true,
    });
    
    // Custom Navigation Events
    $(".nextn").click(function(){
    owl.trigger('owl.next');
    })
    $(".prevp").click(function(){
    owl.trigger('owl.prev');
    })
}); 


$(document).ready(function() {
    var owl = $("#owl-demo2");
    owl.owlCarousel({
    
    navigation : true,
    autoPlay : false,
    slideSpeed : 300,
    paginationSpeed : 400,
    singleItem : false,
    items : 5,
    itemsDesktop : [1170,5],
    itemsDesktopSmall : [1139,4],
    itemsTablet : [1023,3],
    itemsTabletSmall : [767,2],
    itemsMobile : [479,1],
    loop : true,
    //autowidth : true,
    });
    
    // Custom Navigation Events
    $(".next2").click(function(){
    owl.trigger('owl.next');
    })
    $(".prev2").click(function(){
    owl.trigger('owl.prev');
    })
}); 



/* Back to Top */
$(function(){
    var target, scroll;

    $("a[href*=#link]:not([href=#link])").on("click", function(e) {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            target = $(this.hash);
            target = target.length ? target : $("[id=" + this.hash.slice(1) + "]");

            if (target.length) {
                if (typeof document.body.style.transitionProperty === 'string') {
                    e.preventDefault();
                  
                    var avail = $(document).height() - $(window).height();

                    scroll = target.offset().top;
                  
                    if (scroll > avail) {
                        scroll = avail;
                    }

                    $("html").css({
                        "margin-top" : ( $(window).scrollTop() - scroll ) + "px",
                        "transition" : "1s ease-in-out"
                    }).data("transitioning", true);
                } else {
                    $("html, body").animate({
                        scrollTop: scroll
                    }, 1000);
                    return;
                }
            }
        }
    });

    $("html").on("transitionend webkitTransitionEnd msTransitionEnd oTransitionEnd", function (e) {
        if (e.target == e.currentTarget && $(this).data("transitioning") === true) {
            $(this).removeAttr("style").data("transitioning", false);
            $("html, body").scrollTop(scroll);
            return;
        }
    });


    function scrollToAnchor(aid){
        var aTag = $("#" + aid );
        $('html,body').animate({scrollTop: aTag.offset().top},'slow');
    }

    $('.view-all-products').click(function(e){
        e.preventDefault();
        scrollToAnchor('all-products');
    });

});


/* 
    Overlay for Enquire About this Product
*/

(function() {

    var overlayProd = document.querySelector( 'div.overlay-product' );

    if( overlayProd  ) {

        var triggerBttnProd = document.getElementsByClassName( 'trigger-overlay-product' ),
            closeBttnProd = overlayProd.querySelector( 'button.overlay-close-product' );
            transEndEventNames = {
                'WebkitTransition': 'webkitTransitionEnd',
                'MozTransition': 'transitionend',
                'OTransition': 'oTransitionEnd',
                'msTransition': 'MSTransitionEnd',
                'transition': 'transitionend'
            },
            transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
            support = { transitions : Modernizr.csstransitions };

        function toggleOverlay() {
            if( classie.has( overlayProd, 'open' ) ) {

                classie.remove( overlayProd, 'open' );
                classie.add( overlayProd, 'close' );
                var onEndTransitionFn = function( ev ) {
                    if( support.transitions ) {
                        if( ev.propertyName !== 'visibility' ) return;
                        this.removeEventListener( transEndEventName, onEndTransitionFn );
                    }
                    classie.remove( overlayProd, 'close' );
                };
                if( support.transitions ) {
                    overlayProd.addEventListener( transEndEventName, onEndTransitionFn );
                }
                else {
                    onEndTransitionFn();
                }
            }
            else if( !classie.has( overlayProd, 'close' ) ) {
                classie.add( overlayProd, 'open' );
            }
        }

        for(var i=0;i<triggerBttnProd.length;i++){
            triggerBttnProd[i].addEventListener('click', toggleOverlay );
       }

        closeBttnProd.addEventListener( 'click', toggleOverlay );

    }
        
})();





$(document).ready(function() {
//ACCORDION BUTTON ACTION (ON CLICK DO THE FOLLOWING)
$('.accord-btn').click(function() {

    //REMOVE THE ON CLASS FROM ALL BUTTONS
    $('.accord-btn').removeClass('active');
      
    //NO MATTER WHAT WE CLOSE ALL OPEN SLIDES
    $('.accord-cont').slideUp('normal');

    //IF THE NEXT SLIDE WASN'T OPEN THEN OPEN IT
    if($(this).next().is(':hidden') == true) {
        
        //ADD THE ON CLASS TO THE BUTTON
        $(this).addClass('active');
          
        //OPEN THE SLIDE
        $(this).next().slideDown('normal');
     } 
      
 });
  

/*** REMOVE IF MOUSEOVER IS NOT REQUIRED ***/

//ADDS THE .OVER CLASS FROM THE STYLESHEET ON MOUSEOVER 
$('.accord-btn').mouseover(function() {
    $(this).addClass('over');
    
//ON MOUSEOUT REMOVE THE OVER CLASS
}).mouseout(function() {
    $(this).removeClass('over');                                        
});

/*** END REMOVE IF MOUSEOVER IS NOT REQUIRED ***/

});


$(document).ready(function(){
$.cookieBar({
fixed: true,
bottom: true,
declineButton: true,
message: 'By browsing the site, you\'re agreeing to our cookie policy',
});
$('.grid').masonry({
// options
itemSelector: '.grid-item',
percentagePosition: true
});
});


$(function(){
$("#contact-form").validate({
errorElement: 'span',
submitHandler: function(form) {
var _data = {  'nonce' : '<?php echo wp_create_nonce( "ajax_post_author_credit_nonce" ); ?>', 'action' : 'contact_enquiry' }
$(form).ajaxSubmit({
dataType: 'json',
data: _data,
beforeSubmit: function(arr, $form, options) {
var name = $('#contact-form input[name="name"]').fieldValue();
var phone = $('#contact-form input[name="telephone"]').fieldValue();
var email = $('#contact-form input[name="email"]').fieldValue();
var company = $('#contact-form input[name="company"]').fieldValue();
var enquiry = $('#contact-form textarea').fieldValue();
},
success: function(resp){
$('form#contact-form').fadeOut(function(){
$('#form-contact-thank-you').fadeIn(function(){
setTimeout(function(){
$('.row-offcanvas .sidebar-offcanvas .btn-contact').click();
}, 500);
});
});
},
error: function(resp){
alert('Sorry there was an error with your submission. Please try again later.');
}
});
}
});
});


$(function () {
$('form#subform').submit(function (e) {
e.preventDefault();
$.getJSON(
this.action + "?callback=?",
$(this).serialize(),
function (data) {
if (data.Status === 400) {
alert("Error: " + data.Message);
} else { // 200
$('form#subform').fadeOut(function(){
$('.form-block ').append().html('<h5 id="subformh5">Thank you for signing up to our newsletter.</h5>');
});
$('form#subform')
alert("Success: " + data.Message);
}
});
});

});


$(function(){
     $("#sample-request-form").validate({
         errorElement: 'span',
         submitHandler: function(form) {
          
           var _data = {  'nonce' : '<?php echo wp_create_nonce( "ajax_post_author_credit_nonce" ); ?>', 'action' : 'product_enquiry' }
         
           $(form).ajaxSubmit({
            dataType: 'json',
            data: _data,
            beforeSubmit: function(arr, $form, options) { 
               
                var first_name = $('#sample-request-form input[name="first_name"]').fieldValue(); 
                var last_name = $('#sample-request-form input[name="last_name"]').fieldValue(); 
                var phone = $('#sample-request-form input[name="telephone"]').fieldValue(); 
                var email = $('#sample-request-form input[name="email"]').fieldValue(); 
                var company = $('#sample-request-form input[name="company"]').fieldValue(); 
                var enquiry = $('#sample-request-form textarea').fieldValue(); 

            },
            success: function(resp){
              console.log(resp);
              $('#sample-request-form').fadeOut(function(){
                $('#form-thank-you').fadeIn(function(){
                  setTimeout(function(){
                $('.overlay-close').click();
              }, 2000);
                });
              });
              
            },
            error: function(resp){
              console.log(resp);
              alert('Sorry there was an error with your submission. Please try again later.');
            }
          });
        }

       });
    });

  

