<!doctype html>
<html class="no-js" lang="en">
<head>

     <!-- Meta, Title, Styles, Scripts
     ================================================== -->
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     <title><?php echo wp_title(); ?></title>
     <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/stylesheets/app.css" />
     <script src="<?php echo get_template_directory_uri(); ?>/bower_components/modernizr/modernizr.js"></script>

     <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png" />

     <script type="text/javascript" src="http://fast.fonts.net/jsapi/c497674c-44e9-48c4-87bf-7980d4e0b21d.js"></script>

</head>
<body class="row-offcanvas row-offcanvas-right">


<div class="container-main" id="linktop">
<div class="off-canvas-wrap docs-wrap" data-offcanvas>
<div class="inner-wrap">

     <!-- Header
     ================================================== -->
     <header class="header-main">
     <div class="row">
     <div class="large-12 columns">

          <div class="logo"><a href="/" title="Wilson Power Solutions">
          <img src="<?php echo get_template_directory_uri(); ?>/images/logo-wilson-power-solutions.png" alt="Wilson Power Solutions" /></a></div>


          <div class="nav-bar">
          <a href="#" class="right-off-canvas-toggle menu-btn">
          <span class="icon-bar-bg">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span></span>
          <span class="txt">Menu</span></a>

          <div class="right-off-canvas-menu">
          <nav>
          <ul>
          <li><a <?php if (is_page_template('page-home.php')) {echo 'class="active"'; }?> href="/" id="home">Home</a></li>
          <li><a <?php if (is_page_template('product.php')) {echo 'class="active"'; }?>  href="/products/" id="product">Our Products</a></li>
          <li><a <?php if (is_page_template('page-yoursector.php')) {echo 'class="active"'; }?> href="/your-sector/">Your Sector</a></li>
          <li><a <?php if (is_page_template('page-services.php')) {echo 'class="active"'; }?> href="/services/">Services</a></li>
          <li><a <?php if (is_page_template('page-support.php')) {echo 'class="active"'; }?>  href="/support/">Support</a></li>
          <li><a <?php if (is_page_template('page-about-us.php')) {echo 'class="active"'; }?> href="/about-us/">About Us</a></li>
          <li><a <?php if (is_page_template('home.php')) {echo 'class="active"'; }?> href="/news/">News</a></li>
          <li><a <?php if (is_page('Careers')) {echo 'class="active"'; }?> href="/careers-at-wilson/">Careers</a></li>
          </ul>
          </nav>


          <div class="topbar">
          <div class="call-us"><span>Call Us Now On</span><a href="tel:<?php echo get_field('main_telephone_number','option'); ?>"><?php echo get_field('main_telephone_number','option'); ?></div>

          <a href="mailto:<?php echo get_field('main_contact_email_address', 'option'); ?>" class="email"><span><?php echo get_field('main_contact_email_address','option'); ?></span></a>

          <ul class="social-links">
               <li><a href="<?php echo get_field('linked_in_url','option'); ?>">Linked In</a></li>
          </ul>
          
            <ul class="email">
                 <a href="http://wilsonpower.bluestone98.com?lang=fr">Français</a> | <a href="http://wilsonpower.bluestone98.com?lang=de">Deutsch</a> | <a href="http://wilsonpower.bluestone98.com?lang=ar">عربي</a> 
            </ul>
             
           

          </div>
          </div>
          </div>

     </div>
     </div>
     </header>
