<?php 

 /**
 * Template name: All Products Template
 */
     get_header();
?>
     
     
     <!-- Banner Inner 
     ================================================== -->
     <div class="banner-innerout">
     <div class="banner-inner">
     <div class="row">
          
          <div class="medium-6 columns">
          <figure><img src="<?php echo get_field('all_products_main_image'); ?>" alt="All Products" /></figure>
          </div>
          
          
          <div class="medium-6 columns">
          <aside>
          <h2><?php echo get_field('all_products_title'); ?></h2>
          
          <p><?php echo get_field('all_products_intro'); ?></p>

          <p><a href="#" class="btn view-all-products">View all products ></a></p>

          
          </aside>
          </div>
          
     </div>
     </div>
     </div>
          
     <?php get_template_part('content','products-list'); ?>
     <?php get_template_part('content','testimonials'); ?>
    
<?php 
     get_footer();
?>


</body>
</html>