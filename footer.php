<!-- Contact Popup
================================================== -->
<div class="overlay"></div>
<a class="btn-contact" data-toggle="offcanvas"><span><img src="<?php echo get_template_directory_uri(); ?>/images/img-txt01.png" alt="Contact Us" /></span></a>
<div class="sidebar-offcanvas" id="sidebar" role="navigation">
  <a class="btn-contact" data-toggle="offcanvas"><span><img src="<?php echo get_template_directory_uri(); ?>/images/img-txt01.png" alt="Contact Us" /></span></a>
  <div class="contact-form">
    <div class="logo-icon"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-wps.png" alt="Wilson Power Solutions" /></a></div>
    <h2>Contact Us</h2>
    <p>Fill out the form below to make a general enquiry. One of our team will contact you in the next 48 hours to discuss your enquiry further. If your enquiry is more pressing, call us on <span>+44 (0)113 271 7588.</span></p>
    <div id="form-contact-thank-you">
      <p>Thank you. We have received your request and will be in touch as soon as possible.</p>
    </div>
    <form action="/wp-admin/admin-ajax.php" method="post" id="contact-form">
      <ul>
        <li>
          <label>Your namE <span>*</span></label>
          <input name="name" type="text" class="input required" value="" />
        </li>
        <li>
          <label>Company name</label>
          <input name="company" type="text" class="input required" value="" />
        </li>
        <li>
          <label>Email Address <span>*</span></label>
          <input name="email" type="text" class="input required" value="" />
        </li>
        <li>
          <label>Telephone Number <span>*</span></label>
          <input name="telephone" type="text" class="input required" value="" />
        </li>
        <li>
          <label>Your enquiry <span>*</span></label>
          <textarea name="enquiry" class="textarea"></textarea>
        </li>
      </ul>
      <input name="" type="submit" class="btn" value="Send Enquiry" />
    </form>
  </div>
</div>
<!-- Footer
================================================== -->
<footer class="footer-main">
  <div class="row">
    <div class="large-12 columns">
      <div class="article-out">
        <article>
          <h5>Explore</h5>
          <ul>
            <li><a href="/products/">Our Products</a></li>
            <li><a href="/your-sector/">Your Sector</a></li>
            <li><a href="/services/">Services</a></li>
            <li><a href="/support/">Support</a></li>
            <li><a href="/about/">About</a></li>
          </ul>
        </article>
        <article>
          <h5>Why Wilson</h5>
          <ul>
            <li><a href="/environmental-commitment/">Environmental Commitment</a></li>
            <li><a href="/case-studies/">Case Studies</a></li>
            <li><a href="/testimonials/">Testimonials</a></li>
            <li><a href="/our-history/">Our History</a></li>
          </ul>
        </article>
        <article>
          <h5>Connect</h5>
          <ul class="social-links">
            <li><a href="/news/">News</a></li>
            <li><a href="https://www.linkedin.com/company/wilson-power-solutions/ " class="linkedin">Linked In</a></li>
          </ul>
        </article>
      </div>
      <div class="form-block">
        <h5>Newsletter</h5>
        <p>Enter your email for the latest news and updates</p>
        <form action="https://bluestonedesign.createsend.com/t/r/s/dytijtl/" method="post" id="subform">
          <div class="input-bg"><input name="cm-name" type="text" class="input" placeholder="Name" /></div>
          <div class="input-bg last-child"><input name="cm-dytijtl-dytijtl" type="email" class="input" placeholder="Email" /></div>
          <input name="" type="submit" class="btn" value="Sign Up" />
        </form>
        <!-- 2. Add some JavaScript -->
        <!-- 3. You have an AJAX subscribe form! -->
      </div>
      <div class="footer-info">
        <aside>
          <p>&copy; Wilson Power Solutions 2015 <br />
            Wilson Power Solutions uses cookies. <br />
            By browsing the site, you're agreeing to our <span>cookie policy</span></p>
          </aside>
          <p class="right">
            <a href="/accessibility/">Accessibility</a>
            <a href="/terms-and-conditions/">Terms &amp; Conditions</a>
            <a href="/sitemap/">Sitemap</a></p>
          </div>
          <a href="#linktop" class="arrowtop"><span>Top</span></a>
        </div>
      </div>
    </footer>
  </div>
</div>
</div>
<!-- Script
================================================== -->
<script src="<?php echo get_template_directory_uri(); ?>/bower_components/jquery/dist/jquery.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/bower_components/foundation/js/foundation.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/min/app-min.js"></script>
<script type="text/javascript">
 $( document ).ready(function() {
    $modal = $('.overlay-hugeinc.open').is(':visible'); 
    if ( $modal == true ){ 
        $('body').css('overflow', 'hidden');
    } else {
        $('body').css('overflow', 'scroll');
    }
});

</script>
