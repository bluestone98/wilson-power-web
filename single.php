<?php get_header();?>


  <section class="blog">
<article class="row">
  <!--<div class="large-12 columns">
  <button href="#" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="button dropdown">Dropdown Button</button><br>
  <ul id="drop1" data-dropdown-content class="f-dropdown" aria-hidden="true">
    <li><a href="#">This is a link</a></li>
    <li><a href="#">This is another</a></li>
    <li><a href="#">Yet another</a></li>
  </ul>
  <button href="#" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="button dropdown">Dropdown Button</button><br>
<ul id="drop1" data-dropdown-content class="f-dropdown" aria-hidden="true">
 <li><a href="#">This is a link</a></li>
 <li><a href="#">This is another</a></li>
 <li><a href="#">Yet another</a></li>
</ul>
</div>-->
 <div class="grid-single">
<?php while( have_posts() ) :the_post(); ?>
   <div class="single-blog">
     <div class="cd-timeline-top"></div>
     <div class="grid-item-content">
   <div class="grid-item-content-inner">
     <?php the_title('<h1>','</h1>');?>
   <p><?php the_content(); ?></p>
    </div>
  </div>
     <div class="grid-item-content--bottom"><img src="<?php echo get_template_directory_uri(); ?>/images/calendar.png" alt="Movie">&emsp;<?php echo mysql2date('j M Y', $post->post_date); ?></div>
   </div>
 </div>
<?php endwhile; ?>
  </article>
</section>

<!-- Section - 5
================================================== -->
<div class="section-5">
<div class="row">
<div class="large-12 columns">
<div class="heading-txt">
<h2><?php echo get_field( 'testimonial_title', $postid ); ?></h2>
<p><?php echo get_field( 'testimonial_introduction', $postid ); ?></p>
</div>
</div>
</div>
<div class="testimonials">
<div class="row">
<div class="large-12 columns">
<div class="flexslider flexslider2">
<ul class="slides">
<?php
$args = array(
'post_type' => 'testimonial',
'orderby'   => 'menu_order',
'order'     => 'ASC'
);
$the_query = new WP_Query( $args );
// The Loop
if ( $the_query->have_posts() ) {
while ( $the_query->have_posts() ) {
$the_query->the_post();
$image = get_field('testimonial_logo');
if( !empty($image) ):
$url = $image['url'];
$title = $image['title'];
$alt = $image['alt'];
$image = "<figure><img src=\"" . $url ."\" alt=\"." . $image['alt'] . "\" /></figure>";
else:
$image = "<figure><img src=\"" . get_template_directory_uri() . "/images/photo-testi-01.png\" alt=\"PHOTO\" /></figure>";
endif;
?>
<li>
<aside>
<?php echo $image; ?>
<p><?php echo get_field('testimonial_content'); ?></p>
<p class="cl-name"><?php echo get_field('testimonial_name'); ?></p>
<p class="cl-desi"><?php echo get_field('testimonial_company'); ?></p>
</aside>
</li>
<?php
}
}
?>
</ul>
</div>
</div>
</div>
</div>
</div>
<!-- Section - 6
================================================== -->
<div class="section-6">
<div class="row">
<div class="large-12 columns">
<div id="owl-demo2" class="owl-carousel owl-theme">
<?php
$args = array(
'post_type' => 'home_logos',
'orderby'   => 'menu_order',
'order'     => 'ASC'
);
$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
while ( $the_query->have_posts() ) {
$the_query->the_post();
$image = get_field('bottom_logo_image');
if( !empty($image) ):
$url = $image['url'];
$title = $image['title'];
$alt = $image['alt'];
$size = 'product';
$thumb = $image['sizes'][ $size ];
$width = $image['sizes'][ $size . '-width' ];
$height = $image['sizes'][ $size . '-height' ];
$image = "<figure><img src=\"" . $url ."\" alt=\"." . $alt . "\" /></figure>";
else:
$image = "<figure><img src=\"" . get_template_directory_uri() . "/images/logo-iso.gif\" alt=\"PHOTO\" /></figure>";
endif;
?>
<div class="item"><?php echo $image; ?></div>
<?php
}
}
?>
</div>
</div>
</div>
<?php if($the_query->found_posts > 5): ?>
<div class="navigation">
<a class="previous prev2">Previous</a>
<a class="next next2">Next</a>
</div>
<?php endif; ?>
</div>
<?php
get_footer();
?>


</body>
</html>
