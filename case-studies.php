<?php
/**
* Template name: Case Studies
*/
get_header();?>
<section class="your-sector case-study">
	<!-- Banner Inner
	================================================== -->
	<div class="banner-innerout">
		<div class="banner-inner">
			<div class="row">
				<div class="medium-6 columns">
					<img class="hero-image" src="<?php the_field('intro-image'); ?>" alt=""/>
				</div>
				<div class="medium-6 columns">
					<aside>
						<p class="txt"><?php the_title();?></p>
						<?php the_field('your-sector-intro-text'); ?>
					</aside>
				</div>
			</div>
		</div>
	</div>
	<!-- Section - Three cols
	================================================== -->
	<?php
				$args = array(
					'post_type' => 'case-studies',
					'orderby'   => 'menu_order',
					'order'     => 'ASC'
				);
			$the_query = new WP_Query( $args );
			// The Loop
			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
?>
	<div class="section-col">
			<div class="row">
				<div class="article-cols">
					<div class="large-12 columns">
						<article class="services">

							<div class="small-12 medium-12 large-12 columns second">
								<?php echo get_field('content'); ?>
							</div>

					</article>
				</div>

			</div>
		</div>
	</div>
	<?php
			}
		}
	?>

<?php get_template_part('content','testimonials'); ?>

<?php
get_footer();
?>

</body>
</html>
